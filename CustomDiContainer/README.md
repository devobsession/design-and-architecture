
This project is just an example of how a DI container works.
It is NOT production quality.

I often hear developers say that DI containers are "magical", and this is because they don't understand how they work.
The purpose of this project is to show you that the basics are actually quite simple.
I hope that you can get a brief idea of how it works and improve your overall understanding of containers.
