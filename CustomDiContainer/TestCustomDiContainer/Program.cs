﻿using DiContainer;
using System;
using TestCustomDiContainer.TestObjects;

namespace TestCustomDiContainer
{
    class Program
    {
        static void Main(string[] args)
        {
            CustomDiContainer customDiContainer = new CustomDiContainer();
            customDiContainer.RegisterType<PetHouse>();
            customDiContainer.RegisterTypeAs<IRat, Rat>();
            customDiContainer.RegisterTypeAs<ICat, Cat>();
            customDiContainer.RegisterTypeAs<IDog, Dog>();

            PetHouse petHouse = customDiContainer.Resolve<PetHouse>();
            petHouse.SayHiToPets();

            Console.ReadLine();
        }
    }
}
