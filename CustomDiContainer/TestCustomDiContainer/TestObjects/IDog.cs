﻿namespace TestCustomDiContainer.TestObjects
{
    public interface IDog
    {
        void Talk();
    }
}