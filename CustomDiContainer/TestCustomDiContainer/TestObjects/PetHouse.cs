﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TestCustomDiContainer.TestObjects
{
    public class PetHouse
    {
        private readonly ICat _cat;
        private readonly IRat _rat;
        private readonly IDog _dog;

        public PetHouse(ICat cat, IRat rat, IDog dog)
        {
            _cat = cat;
            _rat = rat;
            _dog = dog;
        }

        public void SayHiToPets()
        {
            _dog.Talk();
            _cat.Talk();
            _rat.Talk();
        }
    }
}
