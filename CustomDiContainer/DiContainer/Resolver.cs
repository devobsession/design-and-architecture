﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DiContainer
{
    public class Resolver
    {
        private readonly Dictionary<Type, Type> _container;
        
        public Resolver(Dictionary<Type, Type> container)
        {
            _container = container;
        }

        public T Resolve<T>()
        {
            return (T)Resolve(typeof(T));
        }

        public object Resolve(Type type)
        {
            Type typeToResolve = SetTypeToResolveFromContainer(type);
            
            return CreateInstanceByResolvingConstructorParameters(typeToResolve);
        }

        private Type SetTypeToResolveFromContainer(Type type)
        {
            try
            {
                return _container[type];
            }
            catch (Exception ex)
            {
                throw new ArgumentException($"Could not resolve type {type}");
            }
        }

        private object CreateInstanceByResolvingConstructorParameters(Type typeToResolve)
        {
            var constructor = typeToResolve.GetConstructors().First();
            var constructorParameters = constructor.GetParameters();

            if (constructorParameters.Length == 0)
            {
                return Activator.CreateInstance(typeToResolve);
            }

            var parameters = new List<object>();
            foreach (var parameterToResolve in constructorParameters)
            {
                parameters.Add(Resolve(parameterToResolve.ParameterType));
            }

            return constructor.Invoke(parameters.ToArray());
        }
    }
}
