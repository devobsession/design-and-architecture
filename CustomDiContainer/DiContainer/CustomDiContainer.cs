﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace DiContainer
{
    public class CustomDiContainer
    {
        private readonly Dictionary<Type, Type> _container = new Dictionary<Type, Type>();

        public void RegisterType<T>()
        {
            _container.Add(typeof(T), typeof(T));

        }
        public void RegisterTypeAs<BaseType, DerivedType>()
        {
            _container.Add(typeof(BaseType), typeof(DerivedType));
        }
        
        public T Resolve<T>()
        {
            return (T)Resolve(typeof(T));
        }

        public object Resolve(Type type)
        {
            return new Resolver(_container).Resolve(type);
        }
        
    }
}
