﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JippoingCyclomaticComplexity
{
    /// <summary>
    /// Cyclomatic Complexity = 6
    /// </summary>
    public class HighCCModel
    {
        public int Id { get; set; }
        public bool IsTrue { get; set; }
        public string Name { get; set; }
    }

    /// <summary>
    /// Cyclomatic Complexity = 1
    /// </summary>
    public class LowCCModel
    {
        public int Id;
        public bool IsTrue;
        public string Name;
    }
}
