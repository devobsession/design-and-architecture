﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JippoingCyclomaticComplexity
{
    public class JippoingIfStatementWithVars
    {
        /// <summary>
        /// Cyclomatic Complexity = 6
        /// </summary>
        /// <returns></returns>
        public int HighCyclomaticComplexity()
        {
            int x = 0;

            if (IsTrue(1) && IsTrue(2) && IsTrue(3) && IsTrue(4) && IsTrue(5))
            {
                x = 1;
            }

            return x;
        }

        /// <summary>
        /// Cyclomatic Complexity = 2
        /// </summary>
        /// <returns></returns>
        public int LowCyclomaticComplexity()
        {
            int x = 0;
            bool isTrue = true;
            isTrue &= IsTrue(1);
            isTrue &= IsTrue(2);
            isTrue &= IsTrue(3);
            isTrue &= IsTrue(4);
            isTrue &= IsTrue(5);
            Console.WriteLine(isTrue);
            if (isTrue)
            {
                x = 1;
            }
            return x;
        }

        private bool IsTrue(int i)
        {
            return true;
        }
    }
}
