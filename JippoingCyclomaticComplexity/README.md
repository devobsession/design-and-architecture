
When using different tool the cyclomatic complexity score would be different on each.
Visual studios built in code analyzer creates some wierd scores sometimes.
This sample includes some examples of cyclomatic complexity scores should probably ignore.
The goal of cyclomatic complexity scores is not to get you to do whatever it takes to get the lowest score but to guide you to relook at your code and make sure it is clean and readble.

To run Visual Studios code analyzer
"Analyze" => "Calculate Code Metrics" => "For Solution"