﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contract
{
    public class AddSomeProductRequest : Request
    {
        public SomeProduct SomeProduct { get; }

        public AddSomeProductRequest(SomeProduct someProduct)
        {
            SomeProduct = someProduct;
        }

        public override void ValidateInput(IMyLogger logger)
        {
            if (SomeProduct is null)
            {
                throw new BadRequestException(logger, this, $"Invalid request - {nameof(SomeProduct)} is required");
            }
        }
    }

    public class AddSomeProductResponse
    {
        public SomeProduct SomeProduct { get; }

        public AddSomeProductResponse(SomeProduct someProduct)
        {
            SomeProduct = someProduct;
        }
    }
}
