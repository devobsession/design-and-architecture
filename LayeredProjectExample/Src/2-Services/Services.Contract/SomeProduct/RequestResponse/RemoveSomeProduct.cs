﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contract
{
    public class RemoveSomeProductRequest : Request
    {
        public Guid SomeProductId { get; }

        public RemoveSomeProductRequest(Guid someProductId)
        {
            SomeProductId = someProductId;
        }

        public override void ValidateInput(IMyLogger logger)
        {
            if (SomeProductId == Guid.Empty)
            {
                throw new BadRequestException(logger, this, $"Invalid request - {nameof(SomeProductId)} is required");
            }
        }
    }

    public class RemoveSomeProductResponse
    {
    }
}
