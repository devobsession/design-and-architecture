﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contract
{
    public class GetAllSomeProductRequest : Request
    {
        public override void ValidateInput(IMyLogger logger)
        {
        }
    }

    public class GetAllSomeProductResponse
    {
        public List<SomeProduct> SomeProducts { get; }

        public GetAllSomeProductResponse(List<SomeProduct> someProducts)
        {
            SomeProducts = someProducts;
        }
    }
}
