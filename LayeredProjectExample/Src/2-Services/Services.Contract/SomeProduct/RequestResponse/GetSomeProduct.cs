﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Services.Contract
{
    public class GetSomeProductRequest : Request
    {
        public Guid SomeProductId { get; }

        public GetSomeProductRequest(Guid someProductId)
        {
            SomeProductId = someProductId;
        }

        public override void ValidateInput(IMyLogger logger)
        {
            if (SomeProductId == Guid.Empty)
            {
                throw new BadRequestException(logger, this, $"Invalid request - {nameof(SomeProductId)} is required");
            }
        }
    }

    public class GetSomeProductResponse
    {
        public SomeProduct SomeProduct { get; }

        public GetSomeProductResponse(SomeProduct someProduct)
        {
            SomeProduct = someProduct;
        }
    }
}
