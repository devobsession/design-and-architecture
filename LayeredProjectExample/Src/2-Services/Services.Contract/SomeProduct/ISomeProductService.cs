﻿using System.Threading.Tasks;

namespace Services.Contract
{
    public interface ISomeProductService
    {
        Task<AddSomeProductResponse> Add(AddSomeProductRequest req);
        Task<GetAllSomeProductResponse> GetAll(GetAllSomeProductRequest req);
        Task<GetSomeProductResponse> Get(GetSomeProductRequest req);
        Task<RemoveSomeProductResponse> Remove(RemoveSomeProductRequest req);
        Task<UpdateSomeProductResponse> Update(UpdateSomeProductRequest req);
    }
}