﻿using Infrastructure.Contract;
using Services.Contract;
using Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services
{
    public class SomeProductService : ISomeProductService
    {
        private readonly ISomeProductRepo _someProductRepo;
        private readonly IMyLogger _myLogger;

        public SomeProductService(ISomeProductRepo someProductRepo, IMyLogger myLogger)
        {
            _someProductRepo = someProductRepo;
            _myLogger = myLogger;
        }

        public async Task<GetAllSomeProductResponse> GetAll(GetAllSomeProductRequest req)
        {
            req.ValidateInput(_myLogger);

            var products = await _someProductRepo.Get();
            if (products == null)
            {
                products = new List<SomeProduct>();
            }

            return new GetAllSomeProductResponse(products);
        }

        public async Task<GetSomeProductResponse> Get(GetSomeProductRequest req)
        {
            req.ValidateInput(_myLogger);

            var product = await _someProductRepo.Get(req.SomeProductId);

            if (product == null)
            {
                throw new NotFoundException($"SomeProduct Not Found: {req.SomeProductId}");
            }

            return new GetSomeProductResponse(product);
        }

        public async Task<AddSomeProductResponse> Add(AddSomeProductRequest req)
        {
            req.ValidateInput(_myLogger);

            if (req.SomeProduct.ProductId == Guid.Empty)
            {
                req.SomeProduct.ProductId = Guid.NewGuid();
            }

            await _someProductRepo.Add(req.SomeProduct);

            return new AddSomeProductResponse(req.SomeProduct);
        }

        public async Task<UpdateSomeProductResponse> Update(UpdateSomeProductRequest req)
        {
            req.ValidateInput(_myLogger);

            var product = await _someProductRepo.Update(req.SomeProduct);

            if (product == null)
            {
                throw new NotFoundException($"SomeProduct Not Found: {req.SomeProduct.ProductId}");
            }

            return new UpdateSomeProductResponse();
        }

        public async Task<RemoveSomeProductResponse> Remove(RemoveSomeProductRequest req)
        {
            req.ValidateInput(_myLogger);

            await _someProductRepo.Remove(req.SomeProductId);

            return new RemoveSomeProductResponse();
        }
    }
}
