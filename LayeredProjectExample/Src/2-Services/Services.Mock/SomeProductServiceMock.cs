﻿using Services.Contract;
using Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Services.Mock
{
    public class SomeProductServiceMock : ISomeProductService
    {
        private readonly IMyLogger _myLogger;

        public SomeProductServiceMock(IMyLogger myLogger)
        {
            _myLogger = myLogger;
        }

        public Task<GetAllSomeProductResponse> GetAll(GetAllSomeProductRequest req)
        {
            req.ValidateInput(_myLogger);

            return Task.FromResult(new GetAllSomeProductResponse(new List<SomeProduct>()
            {
                new SomeProduct()
                {
                    ProductId = Guid.Parse("1B382314-1209-43E3-8509-C392F52EFAF1"),
                    Name = "Test Product 1",
                    Price = 10
                },
                new SomeProduct()
                {
                    ProductId = Guid.Parse("2B382314-1209-43E3-8509-C392F52EFAF2"),
                    Name = "Test Product 2",
                    Price = 20
                },
                new SomeProduct()
                {
                    ProductId = Guid.Parse("3B382314-1209-43E3-8509-C392F52EFAF3"),
                    Name = "Test Product 3",
                    Price = 30
                },
            }));
        }

        public Task<GetSomeProductResponse> Get(GetSomeProductRequest req)
        {
            req.ValidateInput(_myLogger);

            return Task.FromResult(new GetSomeProductResponse(new SomeProduct()
            {
                ProductId = Guid.Parse("1B382314-1209-43E3-8509-C392F52EFAF1"),
                Name = "Test Product 1",
                Price = 10
            }));
        }

        public Task<AddSomeProductResponse> Add(AddSomeProductRequest req)
        {
            req.ValidateInput(_myLogger);

            return Task.FromResult(new AddSomeProductResponse(req.SomeProduct));
        }

        public Task<UpdateSomeProductResponse> Update(UpdateSomeProductRequest req)
        {
            req.ValidateInput(_myLogger);

            return Task.FromResult(new UpdateSomeProductResponse());
        }

        public Task<RemoveSomeProductResponse> Remove(RemoveSomeProductRequest req)
        {
            req.ValidateInput(_myLogger);

            return Task.FromResult(new RemoveSomeProductResponse());
        }
    }
}
