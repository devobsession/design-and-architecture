﻿namespace Shared
{
    public enum AppMode
    {
        Live,
        InMemory,
        MockService
    }

}