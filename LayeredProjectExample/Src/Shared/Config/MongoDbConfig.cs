﻿using System;

namespace Shared
{
    public class MongoDbConfig
    {
        public string ConnectionString { get; set; }
        public string DatabaseName { get; set; }

        public MongoDbConfig()
        {
            DatabaseName = Environment.GetEnvironmentVariable("DB_NAME");
            if (DatabaseName is null)
            {
                Console.WriteLine("DB_NAME not found using default 'someproduct'");
                DatabaseName = "someproduct";
            }

            ConnectionString = Environment.GetEnvironmentVariable("MONGODB_CONNECTIONSTRING");
            if (ConnectionString is null)
            {
                Console.WriteLine("DB_CONNECTIONSTRING not found using default mongodb://adminuser:mypassword@localhost:27017/admin");
                ConnectionString = "mongodb://adminuser:mypassword@localhost:27017/admin";
            }
        }
    }
}