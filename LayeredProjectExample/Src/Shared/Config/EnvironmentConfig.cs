﻿using System;

namespace Shared
{
    public class EnvironmentConfig
    {
        [DiContainerInject]
        public MongoDbConfig MongoConfig { get; set; }

        [DiContainerInject]
        public AppMode AppMode { get; set; }

        public EnvironmentConfig(AppMode? appMode = null)
        {
            if (appMode == null)
            {
                string appModeEnv = Environment.GetEnvironmentVariable("APP_MODE");
                var parsed = Enum.TryParse<AppMode>(appModeEnv, out AppMode parsedAppMode);

                if (parsed == false)
                {
                    Console.WriteLine($"APP_MODE not found using default {AppMode.Live}");
                    AppMode = AppMode.Live;
                }
                else
                {
                    AppMode = parsedAppMode;
                }

                Console.WriteLine($"============== APP_MODE = {AppMode} ==============");
            }
            else
            {
                AppMode = appMode.Value;
            }

            MongoConfig = new MongoDbConfig();
        }
    }

}