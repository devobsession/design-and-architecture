﻿using System;

namespace Shared
{
    public class SomeProduct
    {
        public Guid ProductId { get; set; }
        public string Name { get; set; }
        public int Price { get; set; }
    }
}
