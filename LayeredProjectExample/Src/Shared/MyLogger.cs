﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class MyLogger : IMyLogger
    {
        public void Critical(string message, object requestObj = null)
        {
            Console.WriteLine(message);
        }

        public void Debug(string message, object requestObj = null)
        {
            Console.WriteLine(message);
        }

        public void Error(string message, object requestObj = null)
        {
            Console.WriteLine(message);
        }

        public void Info(string message, object requestObj = null)
        {
            Console.WriteLine(message);
        }

        public void Trace(string message, object requestObj = null)
        {
            Console.WriteLine(message);
        }

        public void Warn(string message, object requestObj = null)
        {
            Console.WriteLine(message);
        }
    }
}
