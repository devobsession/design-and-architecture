﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public interface IMyLogger
    {
        void Trace(string message, object requestObj = null);
        void Debug(string message, object requestObj = null);
        void Info(string message, object requestObj = null);
        void Warn(string message, object requestObj = null);
        void Error(string message, object requestObj = null);
        void Critical(string message, object requestObj = null);
    }
}
