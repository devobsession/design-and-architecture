﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class BadRequestException : Exception
    {
        public BadRequestException(IMyLogger logger, object request = null, string message = "Your input is incorrect.") : base(message)
        {
            logger.Warn(message, request);
        }

        public BadRequestException(string message = "Your input is incorrect.") : base(message)
        {
        }
    }
}
