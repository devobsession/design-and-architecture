﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shared
{
    public class NotFoundException : Exception
    {
        public NotFoundException(IMyLogger logger, object request = null, string message = "Not Found.") : base(message)
        {
            logger.Warn(message, request);
        }

        public NotFoundException(string message = "Not Found.") : base(message)
        {
        }
    }
}
