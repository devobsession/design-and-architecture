﻿using Infrastructure.Contract;
using MongoDB.Driver;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class SomeProductRepo : ISomeProductRepo
    {
        private IMongoCollection<SomeProduct> _someProductCollection;

        public SomeProductRepo(MongoDbContext dbContext)
        {
            _someProductCollection = dbContext.SomeProductCollection;
        }

        public async Task<List<SomeProduct>> Get()
        {
            return await _someProductCollection.Find("{}").ToListAsync();
        }

        public async Task<SomeProduct> Get(Guid productId)
        {
            return await _someProductCollection
           .Find(x => x.ProductId == productId)
           .SingleOrDefaultAsync();
        }

        public async Task Add(SomeProduct someProduct)
        {
            await _someProductCollection.InsertOneAsync(someProduct);
        }

        public async Task<SomeProduct> Update(SomeProduct someProduct)
        {
            var product = await _someProductCollection.FindOneAndReplaceAsync(x => x.ProductId == someProduct.ProductId, someProduct);

            return product;
        }

        public async Task Remove(Guid productId)
        {
            await _someProductCollection.DeleteOneAsync(x => x.ProductId == productId);
        }

    }
}
