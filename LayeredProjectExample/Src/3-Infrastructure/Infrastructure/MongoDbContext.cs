﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization;
using MongoDB.Bson.Serialization.Conventions;
using MongoDB.Bson.Serialization.Serializers;
using MongoDB.Driver;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
    public class MongoDbContext

    {
        private readonly MongoDbConfig _mongoConfig;
        private IMyLogger _logger;

        public IMongoDatabase Database { get; private set; }

        public MongoDbContext(MongoDbConfig mongoConfig, IMyLogger logger)
        {
            _mongoConfig = mongoConfig;
            _logger = logger;
            Setup();
        }

        private void Setup()
        {
            ConventionPack(); // must run first
            RegisterClassMap(); // must run before client setup

            var client = new MongoClient(_mongoConfig.ConnectionString);

            MongoDatabaseSettings mongoDatabaseSettings = new MongoDatabaseSettings()
            {
                ReadPreference = ReadPreference.PrimaryPreferred
            };

            Database = client.GetDatabase(_mongoConfig.DatabaseName, mongoDatabaseSettings);
            var isMongoLive = Database.RunCommandAsync((Command<BsonDocument>)"{ping:1}").Wait(10000);
            if (!isMongoLive)
            {
                _logger.Error("We couldn't establish a connection with the database.");
            }

        }

        private void ConventionPack()
        {
            ConventionPack conventionPack = new ConventionPack
            {
                new CamelCaseElementNameConvention(),
                new IgnoreExtraElementsConvention(true),
                new EnumRepresentationConvention(BsonType.String)
            };
            ConventionRegistry.Register("DbConventions", conventionPack, t => true);
        }

        private void RegisterClassMap()
        {
            // BsonDefaults.GuidRepresentationMode = GuidRepresentationMode.V3; must be used until V3 is made the default
            // http://mongodb.github.io/mongo-csharp-driver/2.11/reference/bson/guidserialization/guidrepresentationmode/guidrepresentationmode/
            BsonDefaults.GuidRepresentationMode = GuidRepresentationMode.V3;
            BsonSerializer.RegisterSerializer(new GuidSerializer(GuidRepresentation.Standard)); // must run first

            if (!BsonClassMap.IsClassMapRegistered(typeof(SomeProduct)))
            {
                BsonClassMap.RegisterClassMap<SomeProduct>(cm =>
                {
                    cm.AutoMap();
                    cm.SetIgnoreExtraElements(true);
                    cm.MapIdMember(x => x.ProductId);
                });
            }
        }

        #region Collections

        private IMongoCollection<SomeProduct> _someProductCollection;
        public IMongoCollection<SomeProduct> SomeProductCollection
        {
            get
            {
                if (_someProductCollection == null)
                {
                    _someProductCollection = Database.GetCollection<SomeProduct>(nameof(SomeProduct));
                }

                return _someProductCollection;
            }
        }

        #endregion

    }
}
