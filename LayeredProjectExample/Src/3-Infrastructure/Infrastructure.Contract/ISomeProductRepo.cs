﻿using Shared;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Infrastructure.Contract
{
    public interface ISomeProductRepo
    {
        Task Add(SomeProduct someProduct);
        Task<List<SomeProduct>> Get();
        Task<SomeProduct> Get(Guid productId);
        Task Remove(Guid productId);
        Task<SomeProduct> Update(SomeProduct someProduct);
    }
}