﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Mock
{
    public class DbContextMock
    {
        public List<SomeProduct> SomeProducts { get; set; }

        public DbContextMock()
        {
            SomeProducts = new List<SomeProduct>();
        }
    }
}
