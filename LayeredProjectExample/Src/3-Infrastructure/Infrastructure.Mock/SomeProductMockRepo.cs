﻿using Infrastructure.Contract;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Infrastructure.Mock
{
    public class SomeProductMockRepo : ISomeProductRepo
    {
        private readonly DbContextMock _dbContext;

        public SomeProductMockRepo(DbContextMock dbContext)
        {
            _dbContext = dbContext;
        }

        public Task<List<SomeProduct>> Get()
        {
            var products = _dbContext.SomeProducts;
            return Task.FromResult(products);
        }

        public Task<SomeProduct> Get(Guid productId)
        {
            var product = _dbContext.SomeProducts.SingleOrDefault(x => x.ProductId == productId);

            return Task.FromResult(product);
        }

        public Task Add(SomeProduct someProduct)
        {
            _dbContext.SomeProducts.Add(someProduct);

            return Task.CompletedTask;
        }

        public Task<SomeProduct> Update(SomeProduct someProduct)
        {
            var product = _dbContext.SomeProducts.SingleOrDefault(x => x.ProductId == someProduct.ProductId);
            if (product is null)
            {
                return Task.FromResult<SomeProduct>(null);
            }
            _dbContext.SomeProducts.Remove(product);
            _dbContext.SomeProducts.Add(someProduct);

            return Task.FromResult(someProduct);
        }

        public Task Remove(Guid productId)
        {
            _dbContext.SomeProducts.RemoveAll(x => x.ProductId == productId);

            return Task.CompletedTask;
        }

    }
}
