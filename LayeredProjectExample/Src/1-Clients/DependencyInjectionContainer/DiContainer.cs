﻿using Autofac;
using Infrastructure;
using Infrastructure.Contract;
using Infrastructure.Mock;
using Services;
using Services.Contract;
using Services.Mock;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DependencyInjectionContainer
{
    public class DiContainer
    {
        public static void Setup(ContainerBuilder builder, EnvironmentConfig environmentConfig)
        {
            var envProps = environmentConfig.GetType().GetProperties().Where(prop => Attribute.IsDefined(prop, typeof(DiContainerInjectAttribute)));
            foreach (var envProp in envProps)
            {
                var envVal = envProp.GetValue(environmentConfig);
                if (envVal != null)
                {
                    builder.RegisterInstance(envVal).As(envProp.PropertyType).SingleInstance();
                }
            }

            builder.RegisterType<MyLogger>().As<IMyLogger>();

            switch (environmentConfig.AppMode)
            {
                case AppMode.Live:
                    LiveServiceSetup(builder);
                    LiveInfrastructureSetup(builder);
                    break;

                case AppMode.MockService:
                    MockServiceSetup(builder);
                    break;

                case AppMode.InMemory:
                    LiveServiceSetup(builder);
                    InMemoryInfrastructureSetup(builder);
                    break;
            }
        }

        private static void LiveServiceSetup(ContainerBuilder builder)
        {
            Console.WriteLine("*****LiveServiceSetup*****");

            builder.RegisterType<SomeProductService>().As<ISomeProductService>();
        }

        private static void MockServiceSetup(ContainerBuilder builder)
        {
            Console.WriteLine("*****MockServiceSetup*****");

            builder.RegisterType<SomeProductServiceMock>().As<ISomeProductService>();
        }

        private static void LiveInfrastructureSetup(ContainerBuilder builder)
        {
            Console.WriteLine("*****LiveInfrastructureSetup*****");

            builder.RegisterType<MongoDbContext>().SingleInstance();
            builder.RegisterType<SomeProductRepo>().As<ISomeProductRepo>();
        }

        private static void InMemoryInfrastructureSetup(ContainerBuilder builder)
        {
            Console.WriteLine("*****InMemoryInfrastructureSetup*****");

            builder.RegisterType<DbContextMock>().SingleInstance();
            builder.RegisterType<SomeProductMockRepo>().As<ISomeProductRepo>();
        }

    }
}
