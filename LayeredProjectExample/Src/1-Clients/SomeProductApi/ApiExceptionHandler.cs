﻿using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Services.Contract;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace SomeProductApi
{
    public class ApiExceptionHandler
    {
        private readonly RequestDelegate _next;

        public ApiExceptionHandler(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext httpContext)
        {
            try
            {
                await _next(httpContext);
            }
            catch(BadRequestException ex)
            {
                await WriteResponse(httpContext, HttpStatusCode.BadRequest, ex.Message );
            }
            catch (NotFoundException ex)
            {
                await WriteResponse(httpContext, HttpStatusCode.NotFound, ex.Message);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                if (httpContext.Response.HasStarted)
                {
                    throw;
                }

                await WriteResponse(httpContext, HttpStatusCode.InternalServerError, "Oops! Something went wrong!");
            }
        }

        private async Task WriteResponse(HttpContext httpContext, HttpStatusCode httpStatusCode, string message)
        {
            httpContext.Response.Clear();
            httpContext.Response.StatusCode = (int)httpStatusCode;
            httpContext.Response.ContentType = @"text/plain";
            await httpContext.Response.WriteAsync(message ?? "oops");
        }

    }
}
