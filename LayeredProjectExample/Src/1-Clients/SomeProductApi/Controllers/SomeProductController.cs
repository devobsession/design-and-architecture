﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services.Contract;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SomeProductApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SomeProductController : ControllerBase
    {
        private readonly ISomeProductService _someProductService;

        public SomeProductController(ISomeProductService someProductService)
        {
            _someProductService = someProductService;
        }

        [HttpGet]
        public async Task<IActionResult> GetAll()
        {
            var res = await _someProductService.GetAll(new GetAllSomeProductRequest());

            return Ok(res.SomeProducts);
        }

        [HttpGet("{someProductId}")]
        public async Task<IActionResult> Get(Guid someProductId)
        {
            var res = await _someProductService.Get(new GetSomeProductRequest(someProductId));

            return Ok(res.SomeProduct);
        }

        [HttpPost]
        public async Task<IActionResult> Add(SomeProduct someProduct)
        {
            var res = await _someProductService.Add(new AddSomeProductRequest(someProduct));

            return Ok(res);
        }

        [HttpPut]
        public async Task<IActionResult> Update(SomeProduct someProduct)
        {
            var res = await _someProductService.Update(new UpdateSomeProductRequest(someProduct));

            return Ok();
        }

        [HttpDelete]
        public async Task<IActionResult> Remove(Guid someProductId)
        {
            var res = await _someProductService.Remove(new RemoveSomeProductRequest(someProductId));

            return Ok();
        }

    }
}
