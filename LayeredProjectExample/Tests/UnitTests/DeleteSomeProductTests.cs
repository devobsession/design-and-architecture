using Autofac;
using DependencyInjectionContainer;
using Infrastructure.Mock;
using NUnit.Framework;
using Services.Contract;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTests
{
    public class DeleteSomeProductTests
    {
        private IContainer _container;
        private DbContextMock _dbContextMock;

        [SetUp]
        public void Setup()
        {
            ContainerBuilder builder = new ContainerBuilder();
            DiContainer.Setup(builder, new EnvironmentConfig(AppMode.InMemory));
            _container = builder.Build();

            _dbContextMock = _container.Resolve<DbContextMock>();

            _dbContextMock.SomeProducts = new List<SomeProduct>()
            {
                new SomeProduct()
                {
                    ProductId = Guid.NewGuid(),
                    Name = "test1",
                    Price = 10
                },
                new SomeProduct()
                {
                    ProductId = Guid.NewGuid(),
                    Name = "test2",
                    Price = 20
                }
            };
        }

        [Test]
        public void C1_Remove()
        {
            // Arrange
            var productToRemove = _dbContextMock.SomeProducts.First();
            var req = new RemoveSomeProductRequest(productToRemove.ProductId);

            // Act
            ISomeProductService someProductService = _container.Resolve<ISomeProductService>();
            var response = someProductService.Remove(req).GetAwaiter().GetResult();

            // Assert
            Assert.NotNull(response);

            var foundSomeProduct = _dbContextMock.SomeProducts.SingleOrDefault(x => x.ProductId == productToRemove.ProductId);
            Assert.Null(foundSomeProduct);
        }

        [Test]
        public void C2_Remove_Idempotent()
        {
            // Arrange
            var productIdToRemove = Guid.NewGuid();
            var req = new RemoveSomeProductRequest(productIdToRemove);

            // Act
            ISomeProductService someProductService = _container.Resolve<ISomeProductService>();
            var response = someProductService.Remove(req).GetAwaiter().GetResult();

            // Assert
            Assert.NotNull(response);

            var foundSomeProduct = _dbContextMock.SomeProducts.SingleOrDefault(x => x.ProductId == productIdToRemove);
            Assert.Null(foundSomeProduct);
        }

        [Test]
        public void Validation1_When_EmptyProductId_Throw_BadRequestException()
        {
            // Arrange     
            var req = new RemoveSomeProductRequest(Guid.Empty);

            // Act
            ISomeProductService someProductService = _container.Resolve<ISomeProductService>();
            Action act = () =>
            {
                var response = someProductService.Remove(req).GetAwaiter().GetResult();
            };

            // Assert     
            Assert.Catch<BadRequestException>(() => act());
        }
    }
}