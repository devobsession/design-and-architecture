using Autofac;
using DependencyInjectionContainer;
using Infrastructure.Mock;
using NUnit.Framework;
using Services.Contract;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTests
{
    public class UpdateSomeProductTests
    {
        private IContainer _container;
        private DbContextMock _dbContextMock;

        [SetUp]
        public void Setup()
        {
            ContainerBuilder builder = new ContainerBuilder();
            DiContainer.Setup(builder, new EnvironmentConfig(AppMode.InMemory));
            _container = builder.Build();

            _dbContextMock = _container.Resolve<DbContextMock>();

            _dbContextMock.SomeProducts = new List<SomeProduct>()
            {
                new SomeProduct()
                {
                    ProductId = Guid.NewGuid(),
                    Name = "test1",
                    Price = 10
                },
                new SomeProduct()
                {
                    ProductId = Guid.NewGuid(),
                    Name = "test2",
                    Price = 20
                }
            };
        }

        [Test]
        public void C1_Update()
        {
            // Arrange
            var productToGet = _dbContextMock.SomeProducts.First();
            var toUpdateProduct = new SomeProduct()
            {
                ProductId = productToGet.ProductId,
                Name = "updated",
                Price = 999
            };

            var req = new UpdateSomeProductRequest(toUpdateProduct);

            // Act
            ISomeProductService someProductService = _container.Resolve<ISomeProductService>();
            var response = someProductService.Update(req).GetAwaiter().GetResult();

            // Assert
            Assert.NotNull(response);

            var foundSomeProduct = _dbContextMock.SomeProducts.SingleOrDefault(x => x.ProductId == toUpdateProduct.ProductId);
            Assert.AreEqual(toUpdateProduct.ProductId, foundSomeProduct.ProductId);
            Assert.AreEqual(toUpdateProduct.Name, foundSomeProduct.Name);
            Assert.AreEqual(toUpdateProduct.Price, foundSomeProduct.Price);
        }

        [Test]
        public void Error1_When_NotFoundProduct_Throw_NotFoundException()
        {
            // Arrange     
            _dbContextMock.SomeProducts = new List<SomeProduct>();
            var toUpdateProduct = new SomeProduct()
            {
                ProductId = Guid.NewGuid(),
                Name = "updated",
                Price = 999
            };
            var req = new UpdateSomeProductRequest(toUpdateProduct);

            // Act
            ISomeProductService someProductService = _container.Resolve<ISomeProductService>();
            Action act = () =>
            {
                var response = someProductService.Update(req).GetAwaiter().GetResult();
            };

            // Assert     
            Assert.Catch<NotFoundException>(() => act());
        }

        [Test]
        public void Validation1_When_NullProduct_Throw_BadRequestException()
        {
            // Arrange     
            var req = new UpdateSomeProductRequest(null);

            // Act
            ISomeProductService someProductService = _container.Resolve<ISomeProductService>();
            Action act = () =>
            {
                var response = someProductService.Update(req).GetAwaiter().GetResult();
            };

            // Assert     
            Assert.Catch<BadRequestException>(() => act());
        }
    }
}