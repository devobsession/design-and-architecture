using Autofac;
using DependencyInjectionContainer;
using Infrastructure.Mock;
using NUnit.Framework;
using Services.Contract;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTests
{
    public class GetSomeProductTests
    {
        private IContainer _container;
        private DbContextMock _dbContextMock;

        [SetUp]
        public void Setup()
        {
            ContainerBuilder builder = new ContainerBuilder();
            DiContainer.Setup(builder, new EnvironmentConfig(AppMode.InMemory));
            _container = builder.Build();

            _dbContextMock = _container.Resolve<DbContextMock>();

            _dbContextMock.SomeProducts = new List<SomeProduct>()
            {
                new SomeProduct()
                {
                    ProductId = Guid.NewGuid(),
                    Name = "test1",
                    Price = 10
                },
                new SomeProduct()
                {
                    ProductId = Guid.NewGuid(),
                    Name = "test2",
                    Price = 20
                }
            };
        }

        [Test]
        public void C1_Get()
        {
            // Arrange
            var productToGet = _dbContextMock.SomeProducts.First();
            var req = new GetSomeProductRequest(productToGet.ProductId);

            // Act
            ISomeProductService someProductService = _container.Resolve<ISomeProductService>();
            var response = someProductService.Get(req).GetAwaiter().GetResult();

            // Assert
            Assert.NotNull(response?.SomeProduct);
            Assert.AreEqual(productToGet.ProductId, response.SomeProduct.ProductId);
            Assert.AreEqual(productToGet.Name, response.SomeProduct.Name);
            Assert.AreEqual(productToGet.Price, response.SomeProduct.Price);
        }

        [Test]
        public void Error1_When_NotFoundProduct_Throw_NotFoundException()
        {
            // Arrange     
            _dbContextMock.SomeProducts = new List<SomeProduct>();
                var req = new GetSomeProductRequest(Guid.NewGuid());

            // Act
            ISomeProductService someProductService = _container.Resolve<ISomeProductService>();
            Action act = () =>
            {
                var response = someProductService.Get(req).GetAwaiter().GetResult();
            };

            // Assert     
            Assert.Catch<NotFoundException>(() => act());
        }

        [Test]
        public void Validation1_When_EmptyProductId_Throw_BadRequestException()
        {
            // Arrange     
            var req = new GetSomeProductRequest(Guid.Empty);

            // Act
            ISomeProductService someProductService = _container.Resolve<ISomeProductService>();
            Action act = () =>
            {
                var response = someProductService.Get(req).GetAwaiter().GetResult();
            };

            // Assert     
            Assert.Catch<BadRequestException>(() => act());
        }
    }
}