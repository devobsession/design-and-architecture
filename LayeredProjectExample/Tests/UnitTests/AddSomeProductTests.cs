using Autofac;
using DependencyInjectionContainer;
using Infrastructure.Mock;
using NUnit.Framework;
using Services.Contract;
using Shared;
using System;
using System.Linq;

namespace UnitTests
{
    public class AddSomeProductTests
    {
        private IContainer _container;
        private DbContextMock _dbContextMock;

        [SetUp]
        public void Setup()
        {
            ContainerBuilder builder = new ContainerBuilder();
            DiContainer.Setup(builder, new EnvironmentConfig(AppMode.InMemory));
            _container = builder.Build();

            _dbContextMock = _container.Resolve<DbContextMock>();
        }

        [Test]
        public void C1_Add_SavesToRepo_GeneratesIdIfNull()
        {
            // Arrange      
            var someProduct = new SomeProduct()
            { 
                Name = "test",
                Price = 10
            };
            var req = new AddSomeProductRequest(someProduct);

            // Act
            ISomeProductService someProductService = _container.Resolve<ISomeProductService>();
            var response = someProductService.Add(req).GetAwaiter().GetResult();

            // Assert
            Assert.NotNull(response?.SomeProduct);
            Assert.AreNotEqual(Guid.Empty, response.SomeProduct.ProductId);

            var foundSomeProduct = _dbContextMock.SomeProducts.SingleOrDefault(x => x.ProductId == response.SomeProduct.ProductId);
            Assert.NotNull(foundSomeProduct);
        }

        [Test]
        public void C2_Add_SavesToRepo_UsesIdIfNotNull()
        {
            // Arrange      
            var someProduct = new SomeProduct()
            {
                ProductId = Guid.NewGuid(),
                Name = "test",
                Price = 10
            };
            var req = new AddSomeProductRequest(someProduct);

            // Act
            ISomeProductService someProductService = _container.Resolve<ISomeProductService>();
            var response = someProductService.Add(req).GetAwaiter().GetResult();

            // Assert
            Assert.NotNull(response?.SomeProduct);
            Assert.AreNotEqual(Guid.Empty, response.SomeProduct.ProductId);
            Assert.AreEqual(someProduct.ProductId, response.SomeProduct.ProductId);

            var foundSomeProduct = _dbContextMock.SomeProducts.SingleOrDefault(x => x.ProductId == someProduct.ProductId);
            Assert.NotNull(foundSomeProduct);
        }

        [Test]
        public void Validation1_When_EmptySomeProduct_Throw_BadRequestException()
        {
            // Arrange     
            var req = new AddSomeProductRequest(null);

            // Act
            ISomeProductService someProductService = _container.Resolve<ISomeProductService>();
            Action act = () =>
            {
                var response = someProductService.Add(req).GetAwaiter().GetResult();
            };

            // Assert     
            Assert.Catch<BadRequestException>(() => act());
        }
    }
}