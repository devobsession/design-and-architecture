using Autofac;
using DependencyInjectionContainer;
using Infrastructure.Mock;
using NUnit.Framework;
using Services.Contract;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace UnitTests
{
    public class GetAllSomeProductTests
    {
        private IContainer _container;
        private DbContextMock _dbContextMock;

        [SetUp]
        public void Setup()
        {
            ContainerBuilder builder = new ContainerBuilder();
            DiContainer.Setup(builder, new EnvironmentConfig(AppMode.InMemory));
            _container = builder.Build();

            _dbContextMock = _container.Resolve<DbContextMock>();

            _dbContextMock.SomeProducts = new List<SomeProduct>()
            {
                new SomeProduct()
                {
                    ProductId = Guid.NewGuid(),
                    Name = "test1",
                    Price = 10
                },
                new SomeProduct()
                {
                    ProductId = Guid.NewGuid(),
                    Name = "test2",
                    Price = 20
                },
                 new SomeProduct()
                {
                    ProductId = Guid.NewGuid(),
                    Name = "test3",
                    Price = 30
                }
            };
        }

        [Test]
        public void C1_GetAll()
        {
            // Arrange
            var req = new GetAllSomeProductRequest();

            // Act
            ISomeProductService someProductService = _container.Resolve<ISomeProductService>();
            var response = someProductService.GetAll(req).GetAwaiter().GetResult();

            // Assert
            Assert.NotNull(response?.SomeProducts);
            Assert.AreEqual(_dbContextMock.SomeProducts.Count, response.SomeProducts.Count);
        }

        [Test]
        public void C2_GetAll_WhenNotFound_ReturnEmptyList()
        {
            // Arrange
            _dbContextMock.SomeProducts = new List<SomeProduct>();
            var req = new GetAllSomeProductRequest();

            // Act
            ISomeProductService someProductService = _container.Resolve<ISomeProductService>();
            var response = someProductService.GetAll(req).GetAwaiter().GetResult();

            // Assert
            Assert.NotNull(response?.SomeProducts);
            Assert.AreEqual(0, response.SomeProducts.Count);
        }
    }
}