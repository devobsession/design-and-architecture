﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogicalGrouping.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class TeachersController : ControllerBase
    {
        private readonly ITeacherService _teacherService;

        public TeachersController(ITeacherService teacherService)
        {
            _teacherService = teacherService;
        }

        [HttpGet]
        public IActionResult GetTeachers()
        {
            var teachers = _teacherService.GetTeachers();

            return Ok(teachers);
        }

        [HttpGet("{id}")]
        public IActionResult GetTeacher(int id)
        {
            var teacher = _teacherService.GetTeacher(id);

            return Ok(teacher);
        }

        [HttpPost]
        public IActionResult AddTeacher(Teacher teacher)
        {
           var addedTeacher = _teacherService.AddTeacher(teacher);

            return Ok(addedTeacher);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateTeacher(int id, Teacher teacher)
        {
            teacher.Id = id;
            _teacherService.UpdateTeacher(teacher);

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteTeacher(int id)
        {
            _teacherService.DeleteTeacher(id);

            return Ok();
        }

    }
}
