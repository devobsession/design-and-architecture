﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogicalGrouping.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ClassroomsController : ControllerBase
    {
        private readonly IClassroomService _classroomService;

        public ClassroomsController(IClassroomService classroomService)
        {
            _classroomService = classroomService;
        }

        [HttpGet]
        public IActionResult GetClassrooms()
        {
            var classrooms = _classroomService.GetClassrooms();

            return Ok(classrooms);
        }

        [HttpGet("{id}")]
        public IActionResult GetClassroom(int id)
        {
            var classroom = _classroomService.GetClassroom(id);

            return Ok(classroom);
        }

        [HttpPost]
        public IActionResult AddClassroom(Classroom classroom)
        {
           var addedClassroom = _classroomService.AddClassroom(classroom);

            return Ok(addedClassroom);
        }

        [HttpPut("{id}")]
        public IActionResult UpdateClassroom(int id ,Classroom classroom)
        {
            classroom.Id = id;
            _classroomService.UpdateClassroom(classroom);

            return Ok();
        }

        [HttpDelete("{id}")]
        public IActionResult DeleteClassroom(int id)
        {
            _classroomService.DeleteClassroom(id);

            return Ok();
        }

    }
}
