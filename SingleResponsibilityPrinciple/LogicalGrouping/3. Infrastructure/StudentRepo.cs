﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogicalGrouping
{
    public class StudentRepo : IStudentRepo
    {
        private readonly List<Student> _students;

        public StudentRepo()
        {
            _students = new List<Student>();
        }

        public List<Student> GetStudents()
        {
            return _students;
        }

        public Student GetStudent(int id)
        {
            return _students.SingleOrDefault(x => x.Id == id);
        }

        public Student AddStudent(Student student)
        {
            student.Id = _students.Count + 1;

            _students.Add(student);

            return student;
        }

        public void UpdateStudent(Student student)
        {
            _students.RemoveAll(x => x.Id == student.Id);
            _students.Add(student);
        }

        public void DeleteStudent(int id)
        {
            _students.RemoveAll(x => x.Id == id);
        }
    }
}
