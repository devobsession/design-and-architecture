﻿using System.Collections.Generic;

namespace LogicalGrouping
{
    public interface IStudentRepo
    {
        Student AddStudent(Student student);
        void DeleteStudent(int id);
        Student GetStudent(int id);
        List<Student> GetStudents();
        void UpdateStudent(Student student);
    }
}