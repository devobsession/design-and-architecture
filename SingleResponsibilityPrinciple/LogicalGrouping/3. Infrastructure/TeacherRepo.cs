﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogicalGrouping
{
    public class TeacherRepo : ITeacherRepo
    {
        private readonly List<Teacher> _teachers;

        public TeacherRepo()
        {
            _teachers = new List<Teacher>();
        }

        public List<Teacher> GetTeachers()
        {
            return _teachers;
        }

        public Teacher GetTeacher(int id)
        {
            return _teachers.SingleOrDefault(x => x.Id == id);
        }

        public Teacher AddTeacher(Teacher teacher)
        {
            teacher.Id = _teachers.Count + 1;

            _teachers.Add(teacher);

            return teacher;
        }

        public void UpdateTeacher(Teacher teacher)
        {
            _teachers.RemoveAll(x => x.Id == teacher.Id);
            _teachers.Add(teacher);
        }

        public void DeleteTeacher(int id)
        {
            _teachers.RemoveAll(x => x.Id == id);
        }
    }
}
