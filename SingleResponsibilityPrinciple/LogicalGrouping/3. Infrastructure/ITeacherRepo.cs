﻿using System.Collections.Generic;

namespace LogicalGrouping
{
    public interface ITeacherRepo
    {
        Teacher AddTeacher(Teacher teacher);
        void DeleteTeacher(int id);
        Teacher GetTeacher(int id);
        List<Teacher> GetTeachers();
        void UpdateTeacher(Teacher teacher);
    }
}