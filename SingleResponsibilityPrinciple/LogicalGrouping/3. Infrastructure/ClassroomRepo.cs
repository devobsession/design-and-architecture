﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogicalGrouping
{
    public class ClassroomRepo : IClassroomRepo
    {
        private readonly List<Classroom> _classrooms;

        public ClassroomRepo()
        {
            _classrooms = new List<Classroom>();
        }

        public List<Classroom> GetClassrooms()
        {
            return _classrooms;
        }

        public Classroom GetClassroom(int id)
        {
            return _classrooms.SingleOrDefault(x => x.Id == id);
        }

        public Classroom AddClassroom(Classroom classroom)
        {
            classroom.Id = _classrooms.Count + 1;

            _classrooms.Add(classroom);

            return classroom;
        }

        public void UpdateClassroom(Classroom classroom)
        {
            _classrooms.RemoveAll(x => x.Id == classroom.Id);
            _classrooms.Add(classroom);
        }

        public void DeleteClassroom(int id)
        {
            _classrooms.RemoveAll(x => x.Id == id);
        }
    }
}
