﻿using System.Collections.Generic;

namespace LogicalGrouping
{
    public interface IClassroomService
    {
        Classroom AddClassroom(Classroom classroom);
        void DeleteClassroom(int id);
        Classroom GetClassroom(int id);
        List<Classroom> GetClassrooms();
        void UpdateClassroom(Classroom classroom);
    }
}