﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogicalGrouping
{
    public class StudentService : IStudentService
    {
        private readonly IStudentRepo _studentRepo;

        public StudentService(IStudentRepo studentRepo)
        {
            _studentRepo = studentRepo;
        }

        public List<Student> GetStudents()
        {
            return _studentRepo.GetStudents();
        }

        public Student GetStudent(int id)
        {
            var student = _studentRepo.GetStudent(id);

            if (student == null)
            {
                throw new NotFoundException("Student not found");
            }

            return student;
        }

        public Student AddStudent(Student student)
        {
            ValidateStudent(student);

            return _studentRepo.AddStudent(student);
        }

        public void UpdateStudent(Student student)
        {
            ValidateStudent(student);

            var existingStudent = _studentRepo.GetStudent(student.Id);
            if (existingStudent == null)
            {
                throw new NotFoundException("Student not found");
            }

            _studentRepo.UpdateStudent(student);
        }

        public void DeleteStudent(int id)
        {
            _studentRepo.DeleteStudent(id);
        }

        private static void ValidateStudent(Student student)
        {
            if (string.IsNullOrEmpty(student.FirstName))
            {
                throw new InvalidModelException("First Name is invalid");
            }

            if (string.IsNullOrEmpty(student.LastName))
            {
                throw new InvalidModelException("Last Name is invalid");
            }

            if (student.Age < 6)
            {
                throw new InvalidModelException("Age is invalid");
            }
        }

    }
}
