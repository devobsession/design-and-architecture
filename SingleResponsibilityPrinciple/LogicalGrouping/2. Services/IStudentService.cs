﻿using System.Collections.Generic;

namespace LogicalGrouping
{
    public interface IStudentService
    {
        Student AddStudent(Student student);
        void DeleteStudent(int id);
        Student GetStudent(int id);
        List<Student> GetStudents();
        void UpdateStudent(Student student);
    }
}