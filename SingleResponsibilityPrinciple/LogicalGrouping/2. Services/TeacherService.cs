﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogicalGrouping
{
    public class TeacherService : ITeacherService
    {
        private readonly ITeacherRepo _teacherRepo;

        public TeacherService(ITeacherRepo teacherRepo)
        {
            _teacherRepo = teacherRepo;
        }

        public List<Teacher> GetTeachers()
        {
            return _teacherRepo.GetTeachers();
        }

        public Teacher GetTeacher(int id)
        {
            var teacher = _teacherRepo.GetTeacher(id);

            if (teacher == null)
            {
                throw new NotFoundException("Teacher not found");
            }

            return teacher;

        }

        public Teacher AddTeacher(Teacher teacher)
        {
            ValidateTeacher(teacher);

            return _teacherRepo.AddTeacher(teacher);
        }

        public void UpdateTeacher(Teacher teacher)
        {
            ValidateTeacher(teacher);

            var existingTeacher = _teacherRepo.GetTeacher(teacher.Id);
            if (existingTeacher == null)
            {
                throw new NotFoundException("Teacher not found");
            }

            _teacherRepo.UpdateTeacher(teacher);
        }

        public void DeleteTeacher(int id)
        {
            _teacherRepo.DeleteTeacher(id);
        }

        private static void ValidateTeacher(Teacher teacher)
        {
            if (string.IsNullOrEmpty(teacher.FirstName))
            {
                throw new InvalidModelException("First Name is invalid");
            }

            if (string.IsNullOrEmpty(teacher.LastName))
            {
                throw new InvalidModelException("Last Name is invalid");
            }
        }

    }
}
