﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogicalGrouping
{
    public class ClassroomService : IClassroomService
    {
        private readonly IClassroomRepo _classroomRepo;
        private readonly IStudentRepo _studentRepo;
        private readonly ITeacherRepo _teacherRepo;

        public ClassroomService(IClassroomRepo classroomRepo, IStudentRepo studentRepo, ITeacherRepo teacherRepo)
        {
            _classroomRepo = classroomRepo;
            _studentRepo = studentRepo;
            _teacherRepo = teacherRepo;
        }

        public List<Classroom> GetClassrooms()
        {
            return _classroomRepo.GetClassrooms();
        }

        public Classroom GetClassroom(int id)
        {
             var classroom = _classroomRepo.GetClassroom(id);

            if (classroom == null)
            {
                throw new NotFoundException("Classroom not found");
            }

            return classroom;
        }

        public Classroom AddClassroom(Classroom classroom)
        {
            ValidateClassroom(classroom);

            return _classroomRepo.AddClassroom(classroom);
        }

        public void UpdateClassroom(Classroom classroom)
        {
            ValidateClassroom(classroom);

            var existingClassroom = _classroomRepo.GetClassroom(classroom.Id);
            if (existingClassroom == null)
            {
                throw new NotFoundException("Classroom not found");
            }

            _classroomRepo.UpdateClassroom(classroom);
        }

        public void DeleteClassroom(int id)
        {
            _classroomRepo.DeleteClassroom(id);
        }

        private void ValidateClassroom(Classroom classroom)
        {
            if (string.IsNullOrEmpty(classroom.Subject))
            {
                throw new InvalidModelException("Subject is invalid");
            }

            if (classroom.Grade < 0 || classroom.Grade > 12)
            {
                throw new InvalidModelException("Grade is invalid");
            }

            var teacher = _teacherRepo.GetTeacher(classroom.TeacherId);
            if (teacher == null)
            {
                throw new InvalidModelException("Teacher Id is invalid");
            }

            var studentIds = _studentRepo.GetStudents()?.Select(x => x.Id);
            if (studentIds == null || classroom.StudentIds.Any(x => studentIds.Any(y => x == y) == false))
            {
                throw new InvalidModelException("One or more student Ids are invalid");
            }
        }

    }
}
