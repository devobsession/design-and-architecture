﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogicalGrouping
{
    public class Classroom
    {
        public int Id { get; set; }
        public string Subject { get; set; }
        public int Grade { get; set; }
        public int TeacherId { get; set; }
        public List<int> StudentIds { get; set; }
    }
}
