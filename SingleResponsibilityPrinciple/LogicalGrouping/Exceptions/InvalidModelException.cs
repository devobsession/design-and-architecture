﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LogicalGrouping
{
    public class InvalidModelException : Exception
    {
        public InvalidModelException(string message) : base(message)
        {
        }
    }
}
