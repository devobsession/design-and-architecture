﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    public class GetClassroomController : ControllerBase
    {
        private readonly IGetClassroomService _getClassroomService;

        public GetClassroomController(IGetClassroomService getClassroomService)
        {
            _getClassroomService = getClassroomService;
        }

        [HttpGet("Classrooms/{id}")]
        public IActionResult GetClassroom(int id)
        {
            var classroom = _getClassroomService.GetClassroom(id);

            return Ok(classroom);
        }

    }
}
