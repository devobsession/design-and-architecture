﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    public class AddStudentController : ControllerBase
    {
        private readonly IAddStudentService _addStudentService;

        public AddStudentController(IAddStudentService addStudentService)
        {
            _addStudentService = addStudentService;
        }

        [HttpPost("Students")]
        public IActionResult AddStudent(Student student)
        {
           var addedStudent = _addStudentService.AddStudent(student);

            return Ok(addedStudent);
        }

    }
}
