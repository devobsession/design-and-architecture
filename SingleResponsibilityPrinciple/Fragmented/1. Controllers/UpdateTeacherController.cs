﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    public class UpdateTeacherController : ControllerBase
    {
        private readonly IUpdateTeacherService _updateTeacherService;

        public UpdateTeacherController(IUpdateTeacherService updateTeacherService)
        {
            _updateTeacherService = updateTeacherService;
        }

        [HttpPut("Teachers/{id}")]
        public IActionResult UpdateTeacher(int id, Teacher teacher)
        {
            teacher.Id = id;
            _updateTeacherService.UpdateTeacher(teacher);

            return Ok();
        }

    }
}
