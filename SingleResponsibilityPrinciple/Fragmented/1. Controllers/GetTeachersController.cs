﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    public class GetTeachersController : ControllerBase
    {
        private readonly IGetTeachersService _getTeachersService;

        public GetTeachersController(IGetTeachersService getTeachersService)
        {
            _getTeachersService = getTeachersService;
        }

        [HttpGet("Teachers")]
        public IActionResult GetTeachers()
        {
            var teachers = _getTeachersService.GetTeachers();

            return Ok(teachers);
        }

    }
}
