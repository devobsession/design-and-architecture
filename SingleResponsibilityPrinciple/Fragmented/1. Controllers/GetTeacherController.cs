﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class GetTeacherController : ControllerBase
    {
        private readonly IGetTeacherService _getTeacherService;

        public GetTeacherController(IGetTeacherService getTeacherService)
        {
            _getTeacherService = getTeacherService;
        }

        [HttpGet("{id}")]
        public IActionResult GetTeacher(int id)
        {
            var teacher = _getTeacherService.GetTeacher(id);

            return Ok(teacher);
        }

    }
}
