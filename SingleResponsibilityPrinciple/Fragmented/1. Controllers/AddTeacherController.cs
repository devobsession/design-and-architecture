﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    public class AddTeacherController : ControllerBase
    {
        private readonly IAddTeacherService _addTeacherService;

        public AddTeacherController(IAddTeacherService addTeacherService)
        {
            _addTeacherService = addTeacherService;
        }

        [HttpPost("Teachers")]
        public IActionResult AddTeacher(Teacher teacher)
        {
           var addedTeacher = _addTeacherService.AddTeacher(teacher);

            return Ok(addedTeacher);
        }

    }
}
