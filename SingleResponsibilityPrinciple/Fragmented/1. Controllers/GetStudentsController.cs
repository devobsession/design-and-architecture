﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    public class GetStudentsController : ControllerBase
    {
        private readonly IGetStudentsService _getStudentsService;

        public GetStudentsController(IGetStudentsService getStudentsService)
        {
            _getStudentsService = getStudentsService;
        }

        [HttpGet("Students")]
        public IActionResult GetStudents()
        {
            var students = _getStudentsService.GetStudents();

            return Ok(students);
        }

    }
}
