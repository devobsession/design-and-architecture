﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    public class GetStudentController : ControllerBase
    {
        private readonly IGetStudentService _getStudentService;

        public GetStudentController(IGetStudentService getStudentService)
        {
            _getStudentService = getStudentService;
        }

        [HttpGet("Students/{id}")]
        public IActionResult GetStudent(int id)
        {
            var student = _getStudentService.GetStudent(id);

            return Ok(student);
        }

    }
}
