﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    public class UpdateStudentController : ControllerBase
    {
        private readonly IUpdateStudentService _updateStudentService;

        public UpdateStudentController(IUpdateStudentService updateStudentService)
        {
            _updateStudentService = updateStudentService;
        }

        [HttpPut("Students/{id}")]
        public IActionResult UpdateStudent(int id, Student student)
        {
            student.Id = id;
            _updateStudentService.UpdateStudent(student);

            return Ok();
        }

    }
}
