﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    public class DeleteStudentController : ControllerBase
    {
        private readonly IDeleteStudentService _deleteStudentService;

        public DeleteStudentController(IDeleteStudentService deleteStudentService)
        {
            _deleteStudentService = deleteStudentService;
        }

        [HttpDelete("Students/{id}")]
        public IActionResult DeleteStudent(int id)
        {
            _deleteStudentService.DeleteStudent(id);

            return Ok();
        }

    }
}
