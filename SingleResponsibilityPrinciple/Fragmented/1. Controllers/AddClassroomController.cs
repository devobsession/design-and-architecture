﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    public class AddClassroomController : ControllerBase
    {
        private readonly IAddClassroomService _addClassroomService;

        public AddClassroomController(IAddClassroomService addClassroomService)
        {
            _addClassroomService = addClassroomService;
        }

        [HttpPost("Classrooms")]
        public IActionResult AddClassroom(Classroom classroom)
        {
           var addedClassroom = _addClassroomService.AddClassroom(classroom);

            return Ok(addedClassroom);
        }

    }
}
