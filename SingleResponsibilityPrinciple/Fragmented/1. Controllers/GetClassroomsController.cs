﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    public class GetClassroomsController : ControllerBase
    {
        private readonly IGetClassroomsService _getClassroomsService;

        public GetClassroomsController(IGetClassroomsService getClassroomsService)
        {
            _getClassroomsService = getClassroomsService;
        }

        [HttpGet("Classrooms")]
        public IActionResult GetClassrooms()
        {
            var classrooms = _getClassroomsService.GetClassrooms();

            return Ok(classrooms);
        }

    }
}
