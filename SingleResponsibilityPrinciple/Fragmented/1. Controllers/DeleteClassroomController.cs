﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    public class DeleteClassroomController : ControllerBase
    {
        private readonly IDeleteClassroomService _deleteClassroomService;

        public DeleteClassroomController(IDeleteClassroomService deleteClassroomService)
        {
            _deleteClassroomService = deleteClassroomService;
        }

        [HttpDelete("Classrooms/{id}")]
        public IActionResult DeleteClassroom(int id)
        {
            _deleteClassroomService.DeleteClassroom(id);

            return Ok();
        }

    }
}
