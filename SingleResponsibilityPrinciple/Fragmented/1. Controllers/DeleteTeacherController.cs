﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    public class DeleteTeacherController : ControllerBase
    {
        private readonly IDeleteTeacherService _deleteTeacherService;

        public DeleteTeacherController(IDeleteTeacherService deleteTeacherService)
        {
            _deleteTeacherService = deleteTeacherService;
        }

        [HttpDelete("Teachers/{id}")]
        public IActionResult DeleteTeacher(int id)
        {
            _deleteTeacherService.DeleteTeacher(id);

            return Ok();
        }
    }
}
