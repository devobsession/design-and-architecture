﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented.Controllers
{
    [ApiController]
    public class UpdateClassroomController : ControllerBase
    {
        private readonly IUpdateClassroomService _updateClassroomService;

        public UpdateClassroomController(IUpdateClassroomService updateClassroomService)
        {
            _updateClassroomService = updateClassroomService;
        }

        [HttpPut("Classrooms/{id}")]
        public IActionResult UpdateClassroom(int id, Classroom classroom)
        {
            classroom.Id = id;
            _updateClassroomService.UpdateClassroom(classroom);

            return Ok();
        }
    }
}
