using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddTransient<IGetClassroomsService, GetClassroomsService>();
            services.AddTransient<IGetClassroomService, GetClassroomService>();
            services.AddTransient<IAddClassroomService, AddClassroomService>();
            services.AddTransient<IUpdateClassroomService, UpdateClassroomService>();
            services.AddTransient<IDeleteClassroomService, DeleteClassroomService>();
            services.AddTransient<IGetClassroomsRepo, GetClassroomsRepo>();
            services.AddTransient<IGetClassroomRepo, GetClassroomRepo>();
            services.AddTransient<IAddClassroomRepo, AddClassroomRepo>();
            services.AddTransient<IUpdateClassroomRepo, UpdateClassroomRepo>();
            services.AddTransient<IDeleteClassroomRepo, DeleteClassroomRepo>();
            services.AddSingleton<ClassroomContext>();
            services.AddTransient<IClassroomValidator, ClassroomValidator>();
            services.AddTransient<IClassAttendeesExistValidator, ClassAttendeesExistValidator>();
            services.AddTransient<ISubjectValidator, SubjectValidator>();
            services.AddTransient<IGradeValidator, GradeValidator>();
            services.AddTransient<IGetStudentsService, GetStudentsService>();
            services.AddTransient<IGetStudentService, GetStudentService>();
            services.AddTransient<IAddStudentService, AddStudentService>();
            services.AddTransient<IUpdateStudentService, UpdateStudentService>();
            services.AddTransient<IDeleteStudentService, DeleteStudentService>();
            services.AddTransient<IGetStudentsRepo, GetStudentsRepo>();
            services.AddTransient<IGetStudentRepo, GetStudentRepo>();
            services.AddTransient<IAddStudentRepo, AddStudentRepo>();
            services.AddTransient<IUpdateStudentRepo, UpdateStudentRepo>();
            services.AddTransient<IDeleteStudentRepo, DeleteStudentRepo>();
            services.AddSingleton<StudentContext>();
            services.AddTransient<IStudentValidator, StudentValidator>();
            services.AddTransient<IStudentExistsValidator, StudentExistsValidator>();
            services.AddTransient<IGetTeachersService, GetTeachersService>();
            services.AddTransient<IGetTeacherService, GetTeacherService>();
            services.AddTransient<IAddTeacherService, AddTeacherService>();
            services.AddTransient<IUpdateTeacherService, UpdateTeacherService>();
            services.AddTransient<IDeleteTeacherService, DeleteTeacherService>();
            services.AddTransient<IGetTeachersRepo, GetTeachersRepo>();
            services.AddTransient<IGetTeacherRepo, GetTeacherRepo>();
            services.AddTransient<IAddTeacherRepo, AddTeacherRepo>();
            services.AddTransient<IUpdateTeacherRepo, UpdateTeacherRepo>();
            services.AddTransient<IDeleteTeacherRepo, DeleteTeacherRepo>();
            services.AddSingleton<TeacherContext>();
            services.AddTransient<ITeacherValidator, TeacherValidator>();
            services.AddTransient<ITeacherExistsValidator, TeacherExistsValidator>();
            services.AddTransient<IFirstNameValidator, FirstNameValidator>();
            services.AddTransient<ILastNameValidator, LastNameValidator>();

            services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Fragmented", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Fragmented v1"));
            }

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
