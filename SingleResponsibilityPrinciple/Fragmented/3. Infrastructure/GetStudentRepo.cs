﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class GetStudentRepo : IGetStudentRepo
    {
        private readonly StudentContext _studentContext;

        public GetStudentRepo(StudentContext studentContext)
        {
            _studentContext = studentContext;
        }

        public Student GetStudent(int id)
        {
            return _studentContext.Students.SingleOrDefault(x => x.Id == id);
        }

    }
}
