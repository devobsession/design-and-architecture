﻿using System.Collections.Generic;

namespace Fragmented
{
    public interface IGetTeachersRepo
    {
        List<Teacher> GetTeachers();
    }
}