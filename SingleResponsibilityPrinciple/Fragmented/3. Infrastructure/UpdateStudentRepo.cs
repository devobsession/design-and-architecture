﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class UpdateStudentRepo : IUpdateStudentRepo
    {
        private readonly StudentContext _studentContext;

        public UpdateStudentRepo(StudentContext studentContext)
        {
            _studentContext = studentContext;
        }

        public void UpdateStudent(Student student)
        {
            _studentContext.Students.RemoveAll(x => x.Id == student.Id);
            _studentContext.Students.Add(student);
        }
    }
}
