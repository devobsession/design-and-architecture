﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class DeleteClassroomRepo : IDeleteClassroomRepo
    {
        private readonly ClassroomContext _classroomContext;

        public DeleteClassroomRepo(ClassroomContext classroomContext)
        {
            _classroomContext = classroomContext;
        }

        public void DeleteClassroom(int id)
        {
            _classroomContext.Classrooms.RemoveAll(x => x.Id == id);
        }
    }
}
