﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class AddTeacherRepo : IAddTeacherRepo
    {
        private readonly TeacherContext _teacherContext;

        public AddTeacherRepo(TeacherContext teacherContext)
        {
            _teacherContext = teacherContext;
        }

        public Teacher AddTeacher(Teacher teacher)
        {
            teacher.Id = _teacherContext.Teachers.Count + 1;

            _teacherContext.Teachers.Add(teacher);

            return teacher;
        }

    }
}
