﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class AddClassroomRepo : IAddClassroomRepo
    {
        private readonly ClassroomContext _classroomContext;

        public AddClassroomRepo(ClassroomContext classroomContext)
        {
            _classroomContext = classroomContext;
        }

        public Classroom AddClassroom(Classroom classroom)
        {
            classroom.Id = _classroomContext.Classrooms.Count + 1;

            _classroomContext.Classrooms.Add(classroom);

            return classroom;
        }

    }
}
