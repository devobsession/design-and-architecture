﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class GetClassroomRepo : IGetClassroomRepo
    {
        private readonly ClassroomContext _classroomContext;

        public GetClassroomRepo(ClassroomContext classroomContext)
        {
            _classroomContext = classroomContext;
        }

        public Classroom GetClassroom(int id)
        {
            return _classroomContext.Classrooms.SingleOrDefault(x => x.Id == id);
        }

    }
}
