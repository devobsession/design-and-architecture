﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class DeleteTeacherRepo : IDeleteTeacherRepo
    {
        private readonly TeacherContext _teacherContext;

        public DeleteTeacherRepo(TeacherContext teacherContext)
        {
            _teacherContext = teacherContext;
        }

        public void DeleteTeacher(int id)
        {
            _teacherContext.Teachers.RemoveAll(x => x.Id == id);
        }
    }
}
