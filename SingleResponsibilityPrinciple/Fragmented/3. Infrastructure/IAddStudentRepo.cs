﻿namespace Fragmented
{
    public interface IAddStudentRepo
    {
        Student AddStudent(Student student);
    }
}