﻿namespace Fragmented
{
    public interface IUpdateStudentRepo
    {
        void UpdateStudent(Student student);
    }
}