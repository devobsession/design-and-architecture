﻿namespace Fragmented
{
    public interface IDeleteClassroomRepo
    {
        void DeleteClassroom(int id);
    }
}