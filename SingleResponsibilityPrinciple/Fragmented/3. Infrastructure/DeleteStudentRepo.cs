﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class DeleteStudentRepo : IDeleteStudentRepo
    {
        private readonly StudentContext _studentContext;

        public DeleteStudentRepo(StudentContext studentContext)
        {
            _studentContext = studentContext;
        }

        public void DeleteStudent(int id)
        {
            _studentContext.Students.RemoveAll(x => x.Id == id);
        }
    }
}
