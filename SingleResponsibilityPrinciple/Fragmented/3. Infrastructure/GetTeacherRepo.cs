﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class GetTeacherRepo : IGetTeacherRepo
    {
        private readonly TeacherContext _teacherContext;

        public GetTeacherRepo(TeacherContext teacherContext)
        {
            _teacherContext = teacherContext;
        }

        public Teacher GetTeacher(int id)
        {
            return _teacherContext.Teachers.SingleOrDefault(x => x.Id == id);
        }

    }
}
