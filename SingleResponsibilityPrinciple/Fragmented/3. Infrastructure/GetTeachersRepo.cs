﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class GetTeachersRepo : IGetTeachersRepo
    {
        private readonly TeacherContext _teacherContext;

        public GetTeachersRepo(TeacherContext teacherContext)
        {
            _teacherContext = teacherContext;
        }

        public List<Teacher> GetTeachers()
        {
            return _teacherContext.Teachers;
        }

    }
}
