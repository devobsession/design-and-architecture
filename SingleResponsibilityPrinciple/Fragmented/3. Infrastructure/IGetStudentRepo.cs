﻿namespace Fragmented
{
    public interface IGetStudentRepo
    {
        Student GetStudent(int id);
    }
}