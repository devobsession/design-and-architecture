﻿using System.Collections.Generic;

namespace Fragmented
{
    public interface IGetStudentsRepo
    {
        List<Student> GetStudents();
    }
}