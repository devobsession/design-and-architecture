﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class UpdateTeacherRepo : IUpdateTeacherRepo
    {
        private readonly TeacherContext _teacherContext;

        public UpdateTeacherRepo(TeacherContext teacherContext)
        {
            _teacherContext = teacherContext;
        }

        public void UpdateTeacher(Teacher teacher)
        {
            _teacherContext.Teachers.RemoveAll(x => x.Id == teacher.Id);
            _teacherContext.Teachers.Add(teacher);
        }

    }
}
