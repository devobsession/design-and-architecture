﻿namespace Fragmented
{
    public interface IUpdateClassroomRepo
    {
        void UpdateClassroom(Classroom classroom);
    }
}