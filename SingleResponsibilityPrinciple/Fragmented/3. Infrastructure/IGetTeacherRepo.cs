﻿namespace Fragmented
{
    public interface IGetTeacherRepo
    {
        Teacher GetTeacher(int id);
    }
}