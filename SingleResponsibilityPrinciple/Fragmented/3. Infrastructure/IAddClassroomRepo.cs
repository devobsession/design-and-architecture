﻿namespace Fragmented
{
    public interface IAddClassroomRepo
    {
        Classroom AddClassroom(Classroom classroom);
    }
}