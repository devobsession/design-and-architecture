﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class TeacherContext
    {
        public List<Teacher> Teachers { get; set; } = new List<Teacher>();
    }
}
