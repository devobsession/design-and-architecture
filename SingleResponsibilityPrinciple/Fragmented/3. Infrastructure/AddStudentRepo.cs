﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class AddStudentRepo : IAddStudentRepo
    {
        private readonly StudentContext _studentContext;

        public AddStudentRepo(StudentContext studentContext)
        {
            _studentContext = studentContext;
        }

        public Student AddStudent(Student student)
        {
            student.Id = _studentContext.Students.Count + 1;

            _studentContext.Students.Add(student);

            return student;
        }

    }
}
