﻿namespace Fragmented
{
    public interface IDeleteTeacherRepo
    {
        void DeleteTeacher(int id);
    }
}