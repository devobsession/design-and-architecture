﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class StudentContext
    {
        public List<Student> Students { get; set; } = new List<Student>();
    }
}
