﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class GetStudentsRepo : IGetStudentsRepo
    {
        private readonly StudentContext _studentContext;

        public GetStudentsRepo(StudentContext studentContext)
        {
            _studentContext = studentContext;
        }

        public List<Student> GetStudents()
        {
            return _studentContext.Students;
        }

    }
}
