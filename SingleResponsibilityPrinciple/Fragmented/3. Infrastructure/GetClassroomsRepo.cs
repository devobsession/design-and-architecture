﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class GetClassroomsRepo : IGetClassroomsRepo
    {
        private readonly ClassroomContext _classroomContext;

        public GetClassroomsRepo(ClassroomContext classroomContext)
        {
            _classroomContext = classroomContext;
        }

        public List<Classroom> GetClassrooms()
        {
            return _classroomContext.Classrooms;
        }
    }
}
