﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class ClassroomContext
    {
        public List<Classroom> Classrooms { get; set; }= new List<Classroom>();

    }
}
