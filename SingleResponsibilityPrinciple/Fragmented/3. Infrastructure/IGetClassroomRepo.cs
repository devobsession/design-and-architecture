﻿namespace Fragmented
{
    public interface IGetClassroomRepo
    {
        Classroom GetClassroom(int id);
    }
}