﻿namespace Fragmented
{
    public interface IDeleteStudentRepo
    {
        void DeleteStudent(int id);
    }
}