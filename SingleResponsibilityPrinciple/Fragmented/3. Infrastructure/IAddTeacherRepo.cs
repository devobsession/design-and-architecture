﻿namespace Fragmented
{
    public interface IAddTeacherRepo
    {
        Teacher AddTeacher(Teacher teacher);
    }
}