﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class UpdateClassroomRepo : IUpdateClassroomRepo
    {
        private readonly ClassroomContext _classroomContext;

        public UpdateClassroomRepo(ClassroomContext classroomContext)
        {
            _classroomContext = classroomContext;
        }

        public void UpdateClassroom(Classroom classroom)
        {
            _classroomContext.Classrooms.RemoveAll(x => x.Id == classroom.Id);
            _classroomContext.Classrooms.Add(classroom);
        }

    }
}
