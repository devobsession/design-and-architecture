﻿using System.Collections.Generic;

namespace Fragmented
{
    public interface IGetClassroomsRepo
    {
        List<Classroom> GetClassrooms();
    }
}