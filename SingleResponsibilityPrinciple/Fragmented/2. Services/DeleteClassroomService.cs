﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class DeleteClassroomService : IDeleteClassroomService
    {
        private readonly IDeleteClassroomRepo _deleteClassroomRepo;

        public DeleteClassroomService(IDeleteClassroomRepo deleteClassroomRepo)
        {
            _deleteClassroomRepo = deleteClassroomRepo;
        }

        public void DeleteClassroom(int id)
        {
            _deleteClassroomRepo.DeleteClassroom(id);
        }

    }
}
