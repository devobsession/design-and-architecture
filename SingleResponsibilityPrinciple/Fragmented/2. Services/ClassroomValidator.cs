﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class ClassroomValidator : IClassroomValidator
    {
        private readonly IClassAttendeesExistValidator _validateClassAttendeesExistService;
        private readonly ISubjectValidator _subjectValidator;
        private readonly IGradeValidator _gradeValidator;

        public ClassroomValidator(IClassAttendeesExistValidator validateClassAttendeesExistService, ISubjectValidator subjectValidator, IGradeValidator gradeValidator)
        {
            _validateClassAttendeesExistService = validateClassAttendeesExistService;
            _subjectValidator = subjectValidator;
            _gradeValidator = gradeValidator;
        }

        public void Validate(Classroom classroom)
        {
            _gradeValidator.Validate(classroom.Grade);

            _subjectValidator.Validate(classroom.Subject);

            _validateClassAttendeesExistService.Validate(classroom.StudentIds, classroom.TeacherId);
        }

    }
}
