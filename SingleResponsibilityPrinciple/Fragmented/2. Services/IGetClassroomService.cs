﻿namespace Fragmented
{
    public interface IGetClassroomService
    {
        Classroom GetClassroom(int id);
    }
}