﻿namespace Fragmented
{
    public interface IDeleteClassroomService
    {
        void DeleteClassroom(int id);
    }
}