﻿using System.Collections.Generic;

namespace Fragmented
{
    public interface IGetStudentsService
    {
        List<Student> GetStudents();
    }
}