﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class AddStudentService : IAddStudentService
    {
        private readonly IAddStudentRepo _addStudentRepo;
        private readonly IStudentValidator _studentValidator;

        public AddStudentService(IAddStudentRepo addStudentRepo, IStudentValidator studentValidator)
        {
            _addStudentRepo = addStudentRepo;
            _studentValidator = studentValidator;
        }

        public Student AddStudent(Student student)
        {
            _studentValidator.Validate(student);

            return _addStudentRepo.AddStudent(student);
        }

    }
}
