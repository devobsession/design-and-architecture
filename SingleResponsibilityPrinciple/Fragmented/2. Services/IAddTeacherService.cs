﻿namespace Fragmented
{
    public interface IAddTeacherService
    {
        Teacher AddTeacher(Teacher teacher);
    }
}