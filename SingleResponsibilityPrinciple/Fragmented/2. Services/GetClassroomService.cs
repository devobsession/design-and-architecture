﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class GetClassroomService : IGetClassroomService
    {
        private readonly IGetClassroomRepo _getClassroomRepo;

        public GetClassroomService(IGetClassroomRepo getClassroomRepo)
        {
            _getClassroomRepo = getClassroomRepo;
        }

        public Classroom GetClassroom(int id)
        {
            var classroom = _getClassroomRepo.GetClassroom(id);

            if (classroom == null)
            {
                throw new NotFoundException("Classroom not found");
            }

            return classroom;
        }

    }
}
