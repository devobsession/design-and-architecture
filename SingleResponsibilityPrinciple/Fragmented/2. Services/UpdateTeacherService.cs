﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class UpdateTeacherService : IUpdateTeacherService
    {
        private readonly IUpdateTeacherRepo _updateTeacherRepo;
        private readonly IGetTeacherRepo _getTeacherRepo;
        private readonly ITeacherValidator _teacherValidator;

        public UpdateTeacherService(IUpdateTeacherRepo updateTeacherRepo, IGetTeacherRepo getTeacherRepo, ITeacherValidator teacherValidator)
        {
            _updateTeacherRepo = updateTeacherRepo;
            _getTeacherRepo = getTeacherRepo;
            this._teacherValidator = teacherValidator;
        }

        public void UpdateTeacher(Teacher teacher)
        {
            _teacherValidator.Validate(teacher);

            var existingTeacher = _getTeacherRepo.GetTeacher(teacher.Id);
            if (existingTeacher == null)
            {
                throw new NotFoundException("Teacher not found");
            }

            _updateTeacherRepo.UpdateTeacher(teacher);
        }

    }
}
