﻿namespace Fragmented
{
    public interface ITeacherValidator
    {
        void Validate(Teacher teacher);
    }
}