﻿using System.Collections.Generic;

namespace Fragmented
{
    public interface IGetTeachersService
    {
        List<Teacher> GetTeachers();
    }
}