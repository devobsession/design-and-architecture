﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class LastNameValidator : ILastNameValidator
    {
        public void Validate(string lastName)
        {
            if (string.IsNullOrEmpty(lastName))
            {
                throw new InvalidModelException("Last Name is invalid");
            }
        }
    }
}
