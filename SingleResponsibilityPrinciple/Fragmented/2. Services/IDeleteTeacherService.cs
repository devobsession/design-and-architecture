﻿namespace Fragmented
{
    public interface IDeleteTeacherService
    {
        void DeleteTeacher(int id);
    }
}