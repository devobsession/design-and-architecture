﻿namespace Fragmented
{
    public interface ISubjectValidator
    {
        void Validate(string subject);
    }
}