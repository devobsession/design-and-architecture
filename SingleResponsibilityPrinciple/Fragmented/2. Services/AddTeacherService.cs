﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class AddTeacherService : IAddTeacherService
    {
        private readonly IAddTeacherRepo _addTeacherRepo;
        private readonly ITeacherValidator _teacherValidator;

        public AddTeacherService(IAddTeacherRepo addTeacherRepo, ITeacherValidator teacherValidator)
        {
            _addTeacherRepo = addTeacherRepo;
            _teacherValidator = teacherValidator;
        }

        public Teacher AddTeacher(Teacher teacher)
        {
            _teacherValidator.Validate(teacher);

            return _addTeacherRepo.AddTeacher(teacher);
        }

    }
}
