﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class GetClassroomsService : IGetClassroomsService
    {
        private readonly IGetClassroomsRepo _getClassroomsRepo;

        public GetClassroomsService(IGetClassroomsRepo getClassroomsRepo)
        {
            _getClassroomsRepo = getClassroomsRepo;
        }

        public List<Classroom> GetClassrooms()
        {
            return _getClassroomsRepo.GetClassrooms();
        }

    }
}
