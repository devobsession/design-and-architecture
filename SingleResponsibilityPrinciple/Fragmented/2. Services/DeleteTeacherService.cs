﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class DeleteTeacherService : IDeleteTeacherService
    {
        private readonly IDeleteTeacherRepo _deleteTeacherRepo;

        public DeleteTeacherService(IDeleteTeacherRepo deleteTeacherRepo)
        {
            _deleteTeacherRepo = deleteTeacherRepo;
        }

        public void DeleteTeacher(int id)
        {
            _deleteTeacherRepo.DeleteTeacher(id);
        }

    }
}
