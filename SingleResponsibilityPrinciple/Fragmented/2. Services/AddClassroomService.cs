﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class AddClassroomService : IAddClassroomService
    {
        private readonly IAddClassroomRepo _addClassroomRepo;
        private readonly IClassroomValidator _classroomValidator;

        public AddClassroomService(IAddClassroomRepo addClassroomRepo, IClassroomValidator classroomValidator)
        {
            _addClassroomRepo = addClassroomRepo;
            _classroomValidator = classroomValidator;
        }

        public Classroom AddClassroom(Classroom classroom)
        {
            _classroomValidator.Validate(classroom);

            return _addClassroomRepo.AddClassroom(classroom);
        }

    }
}
