﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class UpdateStudentService : IUpdateStudentService
    {
        private readonly IUpdateStudentRepo _updateStudentRepo;
        private readonly IGetStudentRepo _getStudentRepo;
        private readonly IStudentValidator _studentValidator;

        public UpdateStudentService(IUpdateStudentRepo updateStudentRepo, IGetStudentRepo getStudentRepo, IStudentValidator studentValidator)
        {
            _updateStudentRepo = updateStudentRepo;
            _getStudentRepo = getStudentRepo;
            this._studentValidator = studentValidator;
        }

        public void UpdateStudent(Student student)
        {
            _studentValidator.Validate(student);

            var existingStudent = _getStudentRepo.GetStudent(student.Id);
            if (existingStudent == null)
            {
                throw new NotFoundException("Student not found");
            }

            _updateStudentRepo.UpdateStudent(student);
        }

    }
}
