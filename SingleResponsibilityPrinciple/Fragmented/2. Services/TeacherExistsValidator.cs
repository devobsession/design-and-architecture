﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class TeacherExistsValidator : ITeacherExistsValidator
    {
        private readonly IGetTeacherRepo _getTeacherRepo;

        public TeacherExistsValidator(IGetTeacherRepo getTeacherRepo)
        {
            _getTeacherRepo = getTeacherRepo;
        }

        public void Validate(int teacherId)
        {
            var teacher = _getTeacherRepo.GetTeacher(teacherId);
            if (teacher == null)
            {
                throw new InvalidModelException("Teacher Id is invalid");
            }
        }

    }
}
