﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class SubjectValidator : ISubjectValidator
    {
        public void Validate(string subject)
        {
            if (string.IsNullOrEmpty(subject))
            {
                throw new InvalidModelException("Subject is invalid");
            }
        }

    }
}
