﻿namespace Fragmented
{
    public interface IDeleteStudentService
    {
        void DeleteStudent(int id);
    }
}