﻿namespace Fragmented
{
    public interface IAddClassroomService
    {
        Classroom AddClassroom(Classroom classroom);
    }
}