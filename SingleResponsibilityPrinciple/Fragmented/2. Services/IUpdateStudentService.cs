﻿namespace Fragmented
{
    public interface IUpdateStudentService
    {
        void UpdateStudent(Student student);
    }
}