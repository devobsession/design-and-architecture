﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class ClassAttendeesExistValidator : IClassAttendeesExistValidator
    {
        private readonly IStudentExistsValidator _studentExistsValidator;
        private readonly ITeacherExistsValidator _teacherExistsValidator;

        public ClassAttendeesExistValidator(IStudentExistsValidator studentExistsValidator, ITeacherExistsValidator teacherExistsValidator)
        {
            _studentExistsValidator = studentExistsValidator;
            _teacherExistsValidator = teacherExistsValidator;
        }

        public void Validate(List<int> classroomStudentIds, int teacherId)
        {
            _studentExistsValidator.Validate(classroomStudentIds);
            _teacherExistsValidator.Validate(teacherId);
        }

    }
}
