﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class StudentValidator : IStudentValidator
    {
        private readonly IFirstNameValidator _validateFirstNameService;
        private readonly ILastNameValidator _validateLastNameService;

        public StudentValidator(IFirstNameValidator validateFirstNameService, ILastNameValidator validateLastNameService)
        {
            _validateFirstNameService = validateFirstNameService;
            _validateLastNameService = validateLastNameService;
        }

        public void Validate(Student student)
        {
            _validateFirstNameService.Validate(student.FirstName);

            _validateLastNameService.Validate(student.LastName);
        }

    }
}
