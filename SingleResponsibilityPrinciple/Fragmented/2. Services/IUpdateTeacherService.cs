﻿namespace Fragmented
{
    public interface IUpdateTeacherService
    {
        void UpdateTeacher(Teacher teacher);
    }
}