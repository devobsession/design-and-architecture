﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class UpdateClassroomService : IUpdateClassroomService
    {
        private readonly IUpdateClassroomRepo _updateClassroomRepo;
        private readonly IGetClassroomRepo _getClassroomRepo;
        private readonly IClassroomValidator _classroomValidator;

        public UpdateClassroomService(IUpdateClassroomRepo updateClassroomRepo, IGetClassroomRepo getClassroomRepo, IClassroomValidator classroomValidator)
        {
            _updateClassroomRepo = updateClassroomRepo;
            _getClassroomRepo = getClassroomRepo;
            _classroomValidator = classroomValidator;
        }

        public void UpdateClassroom(Classroom classroom)
        {
            _classroomValidator.Validate(classroom);

            var existingClassroom = _getClassroomRepo.GetClassroom(classroom.Id);
            if (existingClassroom == null)
            {
                throw new NotFoundException("Classroom not found");
            }

            _updateClassroomRepo.UpdateClassroom(classroom);
        }
    }
}
