﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class GetTeacherService : IGetTeacherService
    {
        private readonly IGetTeacherRepo _getTeacherRepo;

        public GetTeacherService(IGetTeacherRepo getTeacherRepo)
        {
            _getTeacherRepo = getTeacherRepo;
        }

        public Teacher GetTeacher(int id)
        {
            var teacher = _getTeacherRepo.GetTeacher(id);

            if (teacher == null)
            {
                throw new NotFoundException("Teacher not found");
            }

            return teacher;

        }

    }
}
