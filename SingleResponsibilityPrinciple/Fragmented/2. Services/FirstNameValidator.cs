﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class FirstNameValidator : IFirstNameValidator
    {
        public void Validate(string firstName)
        {
            if (string.IsNullOrEmpty(firstName))
            {
                throw new InvalidModelException("First Name is invalid");
            }
        }

    }
}
