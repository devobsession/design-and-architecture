﻿namespace Fragmented
{
    public interface IUpdateClassroomService
    {
        void UpdateClassroom(Classroom classroom);
    }
}