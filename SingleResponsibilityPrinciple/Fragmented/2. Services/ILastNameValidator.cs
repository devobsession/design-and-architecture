﻿namespace Fragmented
{
    public interface ILastNameValidator
    {
        void Validate(string lastName);
    }
}