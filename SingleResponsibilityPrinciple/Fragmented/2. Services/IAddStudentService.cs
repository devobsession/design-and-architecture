﻿namespace Fragmented
{
    public interface IAddStudentService
    {
        Student AddStudent(Student student);
    }
}