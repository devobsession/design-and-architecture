﻿using System.Collections.Generic;

namespace Fragmented
{
    public interface IStudentExistsValidator
    {
        void Validate(List<int> classroomStudentIds);
    }
}