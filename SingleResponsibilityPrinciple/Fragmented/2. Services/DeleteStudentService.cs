﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class DeleteStudentService : IDeleteStudentService
    {
        private readonly IDeleteStudentRepo _deleteStudentRepo;

        public DeleteStudentService(IDeleteStudentRepo deleteStudentRepo)
        {
            _deleteStudentRepo = deleteStudentRepo;
        }

        public void DeleteStudent(int id)
        {
            _deleteStudentRepo.DeleteStudent(id);
        }

    }
}
