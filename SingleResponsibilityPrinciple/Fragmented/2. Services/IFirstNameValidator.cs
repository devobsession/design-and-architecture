﻿namespace Fragmented
{
    public interface IFirstNameValidator
    {
        void Validate(string firstName);
    }
}