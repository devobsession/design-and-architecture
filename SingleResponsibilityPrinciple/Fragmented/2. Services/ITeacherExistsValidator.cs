﻿namespace Fragmented
{
    public interface ITeacherExistsValidator
    {
        void Validate(int teacherId);
    }
}