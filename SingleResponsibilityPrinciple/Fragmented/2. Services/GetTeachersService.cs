﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class GetTeachersService : IGetTeachersService
    {
        private readonly IGetTeachersRepo _getTeachersRepo;

        public GetTeachersService(IGetTeachersRepo getTeachersRepo)
        {
            _getTeachersRepo = getTeachersRepo;
        }

        public List<Teacher> GetTeachers()
        {
            return _getTeachersRepo.GetTeachers();
        }

    }
}
