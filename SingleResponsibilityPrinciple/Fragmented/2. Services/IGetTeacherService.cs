﻿namespace Fragmented
{
    public interface IGetTeacherService
    {
        Teacher GetTeacher(int id);
    }
}