﻿namespace Fragmented
{
    public interface IGetStudentService
    {
        Student GetStudent(int id);
    }
}