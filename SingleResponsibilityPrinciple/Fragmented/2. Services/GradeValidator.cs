﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class GradeValidator : IGradeValidator
    {
        public void Validate(int grade)
        {
            if (grade < 0 || grade > 12)
            {
                throw new InvalidModelException("Grade is invalid");
            }
        }

    }
}
