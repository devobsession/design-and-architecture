﻿namespace Fragmented
{
    public interface IGradeValidator
    {
        void Validate(int grade);
    }
}