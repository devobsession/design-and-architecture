﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class GetStudentsService : IGetStudentsService
    {
        private readonly IGetStudentsRepo _getStudentsRepo;

        public GetStudentsService(IGetStudentsRepo getStudentsRepo)
        {
            _getStudentsRepo = getStudentsRepo;
        }

        public List<Student> GetStudents()
        {
            return _getStudentsRepo.GetStudents();
        }

    }
}
