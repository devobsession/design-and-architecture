﻿namespace Fragmented
{
    public interface IClassroomValidator
    {
        void Validate(Classroom classroom);
    }
}