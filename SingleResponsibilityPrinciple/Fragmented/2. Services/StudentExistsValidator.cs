﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class StudentExistsValidator : IStudentExistsValidator
    {
        private readonly IGetStudentsRepo _getStudentsRepo;

        public StudentExistsValidator(IGetStudentsRepo getStudentsRepo)
        {
            _getStudentsRepo = getStudentsRepo;
        }

        public void Validate(List<int> classroomStudentIds)
        {
            var studentIds = _getStudentsRepo.GetStudents()?.Select(x => x.Id);
            if (studentIds == null || classroomStudentIds.Any(x => studentIds.Any(y => x == y) == false))
            {
                throw new InvalidModelException("One or more student Ids are invalid");
            }
        }

    }
}
