﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class TeacherValidator : ITeacherValidator
    {
        private readonly IFirstNameValidator _validateFirstNameService;
        private readonly ILastNameValidator _validateLastNameService;

        public TeacherValidator(IFirstNameValidator validateFirstNameService, ILastNameValidator validateLastNameService)
        {
            _validateFirstNameService = validateFirstNameService;
            _validateLastNameService = validateLastNameService;
        }

        public void Validate(Teacher teacher)
        {
            _validateFirstNameService.Validate(teacher.FirstName);

            _validateLastNameService.Validate(teacher.LastName);
        }

    }
}
