﻿namespace Fragmented
{
    public interface IStudentValidator
    {
        void Validate(Student student);
    }
}