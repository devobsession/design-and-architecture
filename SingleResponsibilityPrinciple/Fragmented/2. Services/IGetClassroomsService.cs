﻿using System.Collections.Generic;

namespace Fragmented
{
    public interface IGetClassroomsService
    {
        List<Classroom> GetClassrooms();
    }
}