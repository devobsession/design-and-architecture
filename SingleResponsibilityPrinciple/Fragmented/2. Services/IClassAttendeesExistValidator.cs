﻿using System.Collections.Generic;

namespace Fragmented
{
    public interface IClassAttendeesExistValidator
    {
        void Validate(List<int> classroomStudentIds, int teacherId);
    }
}