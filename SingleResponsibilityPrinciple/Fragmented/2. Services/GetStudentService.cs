﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class GetStudentService : IGetStudentService
    {
        private readonly IGetStudentRepo _getStudentRepo;

        public GetStudentService(IGetStudentRepo getStudentRepo)
        {
            _getStudentRepo = getStudentRepo;
        }

        public Student GetStudent(int id)
        {
            var student = _getStudentRepo.GetStudent(id);

            if (student == null)
            {
                throw new NotFoundException("Student not found");
            }

            return student;
        }

    }
}
