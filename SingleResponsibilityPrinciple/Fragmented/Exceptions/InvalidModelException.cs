﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Fragmented
{
    public class InvalidModelException : Exception
    {
        public InvalidModelException(string message) : base(message)
        {
        }
    }
}
