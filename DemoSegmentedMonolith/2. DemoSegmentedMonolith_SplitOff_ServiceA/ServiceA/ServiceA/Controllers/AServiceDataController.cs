﻿using A.Services;
using Microsoft.AspNetCore.Mvc;

namespace ServiceA.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class AServiceDataController : ControllerBase
    {
        private readonly IAService _aService;

        public AServiceDataController(IAService aService)
        {
            _aService = aService;
        }

        [HttpGet]
        public string Get()
        {
            return _aService.Get();
        }
    }
}
