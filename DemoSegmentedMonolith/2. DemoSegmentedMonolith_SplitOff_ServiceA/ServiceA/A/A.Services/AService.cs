﻿using System;

namespace A.Services
{
    public class AService : IAService
    {
        private readonly IARepo _aRepo;

        public AService(IARepo aRepo)
        {
            _aRepo = aRepo;
        }

        public string Get()
        {
            // Some Bussiness Logic

            var aModel = _aRepo.Get();

            return aModel.Value;
        }
    }
}
