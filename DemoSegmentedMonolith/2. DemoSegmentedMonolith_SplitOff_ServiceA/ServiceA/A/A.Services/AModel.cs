﻿using System;

namespace A.Services
{
    public class AModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
