﻿using Shared;

namespace Shared
{
    public interface IAService
    {
        string Get();
    }
}