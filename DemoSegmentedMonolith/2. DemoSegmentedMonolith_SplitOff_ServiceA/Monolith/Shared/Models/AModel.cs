﻿using System;

namespace Shared
{
    public class AModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
