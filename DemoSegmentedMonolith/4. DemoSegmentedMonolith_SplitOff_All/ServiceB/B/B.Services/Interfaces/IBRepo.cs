﻿
namespace B.Services
{
    public interface IBRepo
    {
        BModel Get();
    }
}