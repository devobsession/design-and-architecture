﻿
namespace B.Services
{
    public class BService : IBService
    {
        private readonly IBRepo _bRepo;

        public BService(IBRepo bRepo)
        {
            _bRepo = bRepo;
        }

        public string Get()
        {
            // Some Bussiness Logic

            var bModel = _bRepo.Get();

            return bModel.Value;
        }
    }
}
