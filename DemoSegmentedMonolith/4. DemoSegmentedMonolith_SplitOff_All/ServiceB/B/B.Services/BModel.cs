﻿using System;

namespace B.Services
{
    public class BModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
