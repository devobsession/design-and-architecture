﻿using B.Services;
using System.Collections.Generic;
using System.Linq;

namespace B.Infrastructure
{
    public class BRepo : IBRepo
    {
        private List<BModel> _bModels = new List<BModel>()
        {
            new BModel()
            {
                Id = 1,
                Value = "BBBBBBBBBBB"
            }
        };

        public BModel Get()
        {
            return _bModels.First();
        }
    }
}
