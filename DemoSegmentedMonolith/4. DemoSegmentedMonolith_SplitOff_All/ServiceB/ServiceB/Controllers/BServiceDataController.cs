﻿using B.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceB.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class BServiceDataController : ControllerBase
    {
        private readonly IBService _bService;

        public BServiceDataController(IBService bService)
        {
            _bService = bService;
        }

        [HttpGet]
        public string Get()
        {
            return _bService.Get();
        }
    }
}
