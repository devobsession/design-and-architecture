﻿using Shared;
using System.Net.Http;

namespace Monolith
{
    public class AServiceBroker : IAService
    {
        private HttpClient _httpClient;

        public AServiceBroker()
        {
            _httpClient = new HttpClient();
        }
        public string Get()
        {
           return _httpClient.GetStringAsync("http://localhost:5001/AServiceData").GetAwaiter().GetResult();
        }
    }
}
