﻿using Shared;
using System.Net.Http;

namespace Monolith
{
    public class CServiceBroker : ICService
    {
        private HttpClient _httpClient;

        public CServiceBroker()
        {
            _httpClient = new HttpClient();
        }
        public string Get()
        {
           return _httpClient.GetStringAsync("http://localhost:5003/CServiceData").GetAwaiter().GetResult();
        }
    }
}
