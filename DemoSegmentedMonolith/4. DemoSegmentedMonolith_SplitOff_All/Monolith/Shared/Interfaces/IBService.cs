﻿using Shared;

namespace Shared
{
    public interface IBService
    {
        string Get();
    }
}