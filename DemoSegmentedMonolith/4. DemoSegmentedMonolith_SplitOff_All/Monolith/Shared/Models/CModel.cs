﻿using System;

namespace Shared
{
    public class CModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
