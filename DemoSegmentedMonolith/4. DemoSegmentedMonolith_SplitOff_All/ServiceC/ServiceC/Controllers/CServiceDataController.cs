﻿using C.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ServiceC.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class CServiceDataController : ControllerBase
    {
        private readonly ICService _cService;

        public CServiceDataController(ICService cService)
        {
            _cService = cService;
        }

        [HttpGet]
        public string Get()
        {
            return _cService.Get();
        }
    }
}

