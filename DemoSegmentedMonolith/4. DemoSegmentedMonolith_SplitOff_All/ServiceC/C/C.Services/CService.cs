﻿
namespace C.Services
{
    public class CService : ICService
    {
        private readonly ICRepo _cRepo;
        private readonly IBService _bService;

        public CService(ICRepo cRepo, IBService bService)
        {
            _cRepo = cRepo;
            _bService = bService;
        }

        public string Get()
        {
            // Some Bussiness Logic

            var cModel = _cRepo.Get();

            var bValue = _bService.Get();

            return bValue + cModel.Value;
        }
    }
}
