﻿using C.Services;
using System.Collections.Generic;
using System.Linq;

namespace C.Infrastructure
{
    public class CRepo : ICRepo
    {
        private List<CModel> _cModels = new List<CModel>() 
        {
            new CModel()
            {
                Id = 1,
                Value = "CCCCCCCCC"
            }
        };

        public CModel Get()
        {
            return _cModels.First();
        }
    }
}
