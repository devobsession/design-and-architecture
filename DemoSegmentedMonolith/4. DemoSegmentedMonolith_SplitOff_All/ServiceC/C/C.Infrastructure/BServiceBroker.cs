﻿using C.Services;
using System.Net.Http;

namespace C.Infrastructure
{
    public class BServiceBroker : IBService
    {
        private HttpClient _httpClient;

        public BServiceBroker()
        {
            _httpClient = new HttpClient();
        }
        public string Get()
        {
           return _httpClient.GetStringAsync("http://localhost:5002/BServiceData").GetAwaiter().GetResult();
        }
    }
}
