﻿using Shared;

namespace C.Services
{
    public interface ICRepo
    {
        CModel Get();
    }
}