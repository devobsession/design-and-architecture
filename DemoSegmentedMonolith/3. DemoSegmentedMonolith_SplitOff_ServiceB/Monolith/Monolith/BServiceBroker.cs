﻿using Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace Monolith
{
    public class BServiceBroker : IBService
    {
        private HttpClient _httpClient;

        public BServiceBroker()
        {
            _httpClient = new HttpClient();
        }
        public string Get()
        {
           return _httpClient.GetStringAsync("http://localhost:5002/BServiceData").GetAwaiter().GetResult();
        }
    }
}
