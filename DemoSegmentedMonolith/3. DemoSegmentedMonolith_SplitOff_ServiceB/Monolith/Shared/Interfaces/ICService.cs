﻿using Shared;

namespace Shared
{
    public interface ICService
    {
        string Get();
    }
}