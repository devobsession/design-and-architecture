﻿using System;

namespace Shared
{
    public class BModel
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
