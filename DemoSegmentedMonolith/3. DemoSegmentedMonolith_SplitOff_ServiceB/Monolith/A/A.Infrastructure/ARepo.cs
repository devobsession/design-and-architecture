﻿using A.Services;
using Shared;
using System;
using System.Collections.Generic;
using System.Linq;

namespace A.Infrastructure
{
    public class ARepo : IARepo
    {
        private List<AModel> _aModels = new List<AModel>()
        { 
            new AModel()
            {
                Id = 1,
                Value = "AAAAAAAAAA"
            }
        };

        public AModel Get()
        {
            return _aModels.First();
        }

    }
}
