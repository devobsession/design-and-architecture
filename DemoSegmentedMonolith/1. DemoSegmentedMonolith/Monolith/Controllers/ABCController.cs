﻿using Microsoft.AspNetCore.Mvc;
using Shared;

namespace Monolith.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ABCController : ControllerBase
    {
        private readonly IAService _aService;
        private readonly IBService _bService;
        private readonly ICService _cService;

        public ABCController(IAService aService, IBService bService, ICService cService)
        {
            _aService = aService;
            _bService = bService;
            _cService = cService;
        }

        [HttpGet("AB")]
        public string AB()
        {
            var a = _aService.Get();

            var b = _bService.Get();

            return a+b;
        }

        [HttpGet("C")]
        public string C()
        {
            var c = _cService.Get();

            return c;
        }
    }
}
