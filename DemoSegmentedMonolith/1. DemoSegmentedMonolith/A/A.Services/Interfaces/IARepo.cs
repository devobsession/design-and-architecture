﻿using Shared;

namespace A.Services
{
    public interface IARepo
    {
        AModel Get();
    }
}