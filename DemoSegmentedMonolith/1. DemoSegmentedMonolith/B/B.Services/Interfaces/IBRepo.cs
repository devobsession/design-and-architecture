﻿using Shared;

namespace B.Services
{
    public interface IBRepo
    {
        BModel Get();
    }
}